//
//  Message.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/05.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import Foundation
import Firebase

struct Message {
    
    var user: User?
    var id: String?
    let messageText: String?
    let fromId: String?
    let toId: String?
    //let uid: String?
    
    var countNumber: UInt?
    let creationDate: Date
    
    var read: UInt?
    
    init(user: User, dictionary: [String: Any]) {
        self.user = user
        self.messageText = dictionary["messageText"] as? String ?? ""
        self.fromId = dictionary["fromId"] as? String ?? ""
        self.toId = dictionary["toId"] as? String ?? ""
        //self.uid = dictionary["uid"] as? String ?? ""
        
         self.read = dictionary["read"] as? UInt ?? 0
        
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
    }
    
    func chatPartnerId() -> String? {
        if fromId == Auth.auth().currentUser?.uid {
            return toId
        } else {
            return fromId
        }
    }
}
