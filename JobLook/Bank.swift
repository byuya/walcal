//
//  Bank.swift
//  JobLook
//
//  Created by Akiya Ozawa on 12/12/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import Foundation

struct Bank {
    
    let user: User
    
    let last4: String
    let bankId: String
    let bankName: String
    
    init(user: User, dictionary: [String: Any]) {
        self.user = user
        self.last4 = dictionary["last4"] as? String ?? ""
        self.bankId = dictionary["id"] as? String ?? ""
        self.bankName = dictionary["bank_name"] as? String ?? ""
    }
    
}
