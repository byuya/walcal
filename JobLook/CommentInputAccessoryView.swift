//
//  CommentInputAccessoryView.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/03.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

protocol CommentInputAccessoryViewDelegate {
    func didSubmit(for comment: String)
}

class CommentInputAccessoryView: UIView {
    
    var delegate: CommentInputAccessoryViewDelegate?
    
    func commentClearTextField() {
        commentTextView.text = nil
        commentTextView.showPlaceholderLabel()
    }
    
    fileprivate let commentTextView: CommentInputTextView = {
        let tv = CommentInputTextView()
        //tv.placeholder = "コメントを残す"
        tv.isScrollEnabled = false// to make space leager in textView
        //tv.backgroundColor = .red
        tv.font = UIFont.systemFont(ofSize: 18)
        return tv
    }()
    
    fileprivate let submitButton: UIButton = {
        let sb = UIButton(type: .system)
        sb.setTitle("Send", for: .normal)
        sb.setTitleColor(.black, for: .normal)
        sb.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        sb.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        return sb
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //multitext line part 1
        autoresizingMask = .flexibleHeight
        
        backgroundColor = .white
        
        addSubview(submitButton)
        submitButton.anchor(top: topAnchor, left: nil, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 12, height: 50, width: 50)
        
        addSubview(commentTextView)
        ////multitext line part 3 add safeArealayoutGuide
        commentTextView.anchor(top: topAnchor, left: leftAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: submitButton.leftAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 0, height: 0, width: 0)
        
        setupLineSeparatorView()
    }
    //multitext line part 2
    override var intrinsicContentSize: CGSize {
        return .zero
    }
    
    
    fileprivate func setupLineSeparatorView() {
        let lineSeparatoView = UIView()
        lineSeparatoView.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230)
        addSubview(lineSeparatoView)
        lineSeparatoView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
    }
    
    @objc func handleSubmit(){
        print("123")
        
        guard let commentText = commentTextView.text else {return}
        delegate?.didSubmit(for: commentText)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
