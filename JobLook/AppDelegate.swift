//
//  AppDelegate.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/13.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import Stripe
import UserNotifications
import FirebaseMessaging
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        application.applicationIconBadgeNumber = 0
        
        //UNUserNotificationCenter.current().delegate = self
        
        FirebaseApp.configure()
        
        window = UIWindow()
        window?.rootViewController = MainTabBarController()
        
        STPPaymentConfiguration.shared().publishableKey = "pk_test_l3obZhsHSeEFk5ecOns4OyoR"
        
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
        
        attemptRegisterForNotification(application: application)
        
        // Override point for customization after application launch.
        return true
    }
    
    //iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    
        print("entire message here\(userInfo)")
       
        if let badge = userInfo["badge"] as? String, let messageBadge = userInfo["messageBadge"] as? String {
            
            switch  application.applicationState {
                
            case .inactive:
                //application.applicationIconBadgeNumber = badge
                print("make sure this is working in inactive state")
                break
                
            case .background:
                application.applicationIconBadgeNumber = Int(badge) ?? 0
                if let mainTabBarController = window?.rootViewController as? MainTabBarController {
                    
                    if let commentNavigationController = mainTabBarController.viewControllers?[1] as? UINavigationController {
                        commentNavigationController.tabBarItem.badgeValue = messageBadge
                    }
                }
                print("make sure this is working in background")
                break
                
            case .active:
                //application.applicationIconBadgeNumber = Int(badge) ?? 0
                print("make sure this is working in when active state")
                break
            }
            
        }
        
        if let badge = userInfo["badge"] as? String, let commentBadge = userInfo["commentBadge"] as? String {
            
                    switch  application.applicationState {
            
                    case .inactive:
                        //application.applicationIconBadgeNumber = badge
                        print("make sure this is working in inactive state")
                        break
            
                    case .background:
                        application.applicationIconBadgeNumber = Int(badge) ?? 0
                        if let mainTabBarController = window?.rootViewController as? MainTabBarController {
                            
                            if let commentNavigationController = mainTabBarController.viewControllers?[4] as? UINavigationController {
                                commentNavigationController.tabBarItem.badgeValue = commentBadge
                            }
                        }
                        print("make sure this is working in background")
                        break
            
                    case .active:
                        //application.applicationIconBadgeNumber = Int(badge) ?? 0
                        print("make sure this is working in when active state")
                        break
                    }
                
            }
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Registered for notifications", deviceToken)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Registered with FCM with token", fcmToken)
        
    }
     static let passingDataToAddCardController = NSNotification.Name(rawValue: "passingData")
    //listen for user notifications
    //this is working only in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
         completionHandler([.alert, .badge])
        
        let userInfo = notification.request.content.userInfo
    
        print("getting this info", userInfo)
        
        if let fromId = userInfo["fromId"] as? String, let statusCode = userInfo["userInfo"] as? String, let errorMessage = userInfo["errorMessage"] as? String {
            
             NotificationCenter.default.post(name: AppDelegate.passingDataToAddCardController, object: nil, userInfo: ["fromId" : fromId, "statusCode": statusCode, "errorMessage": errorMessage])
        }
        
        if let fromId = userInfo["fromId"] as? String, let badge = userInfo["badge"] as? String, let messageBadge = userInfo["messageBadge"] as? String {
            
            print("this is fromId", fromId, "I will display this", badge, "this is messageBadge", messageBadge)
        
            guard let badge = Int(badge) else {return}
            UIApplication.shared.applicationIconBadgeNumber = badge
            
            if let mainTabBarController = window?.rootViewController as? MainTabBarController {
                //mainTabBarController.selectedIndex = 1
                
                if let homeNavigationController = mainTabBarController.viewControllers?[1] as? UINavigationController {
                    //homeNavigationController.pushViewController(inboxMessengerController, animated: true)
                    homeNavigationController.tabBarItem.badgeValue = messageBadge
                    
                }
            }
        }
        
         if let fromId = userInfo["fromId"] as? String, let badge = userInfo["badge"] as? String, let commentBadge = userInfo["commentBadge"] as? String {
            
              print("this is fromId", fromId, "I will display this", badge, "this is commendBadge", commentBadge)
            
            guard let badge = Int(badge) else {return}
            UIApplication.shared.applicationIconBadgeNumber = badge
            
            if let mainTabBarController = window?.rootViewController as? MainTabBarController {
                
                if let commentNavigationController = mainTabBarController.viewControllers?[4] as? UINavigationController {
                    commentNavigationController.tabBarItem.badgeValue = commentBadge
                }
            }
            
        }
    }
    
    //if background, this callback will not be called untill users tap the notification// 12/27
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        
        //following notification function
        if let followerId = userInfo["followerId"] as? String {
            print(followerId)
            
            //I want to push the UserProfileController for followerId somehow
            
            let userProfileController = UserProfileController(collectionViewLayout: UICollectionViewFlowLayout())
            userProfileController.userId = followerId
            
        //how do we access out main UI from AppDelegate
            if let mainTabBarController = window?.rootViewController as? MainTabBarController {
                
                mainTabBarController.selectedIndex = 0
                mainTabBarController.presentedViewController?.dismiss(animated: true, completion: nil)
                
                if let homeNavigationController = mainTabBarController.viewControllers?.first as? UINavigationController {
                    homeNavigationController.pushViewController(userProfileController, animated: true)
                
                }
            }
        }
        
        // message notification function
        if let fromId = userInfo["fromId"] as? String, let badge = userInfo["badge"] as? String, let messageBadge = userInfo["messageBadge"] as? String {
            print(fromId, badge, messageBadge)
            
            //application.applicationIconBadgeNumber = Int(badge)!
            
            let messageController = MessageController(collectionViewLayout: UICollectionViewFlowLayout())
            messageController.userId = fromId
            //messageController.message = message
            
            if let mainTabBarController = window?.rootViewController as? MainTabBarController {
                mainTabBarController.selectedIndex = 1
                
                if let homeNavigationController = mainTabBarController.viewControllers?[1] as? UINavigationController {
                    homeNavigationController.pushViewController(messageController, animated: true)
                    //homeNavigationController.tabBarItem.badgeValue = "1"
//                    let application = UIApplication.shared
//                    application.applicationIconBadgeNumber = 3

                }
               
            }
        }
        
    }
    
    private func attemptRegisterForNotification(application: UIApplication) {
        print("Attempting to register APNs")
    
       Messaging.messaging().delegate = self
        
        //had to set because of need for calling func userNotificion
        UNUserNotificationCenter.current().delegate = self
        
        //user notificationauth
        // all of this works for ios 10+
        let options: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: options) { (granted, err) in
            if let err = err {
                print("Failed to request auth", err)
                return
                
            }
            
            if granted {
                print("Auth granted.")
            } else {
                print("Auth denied")
            }
        }
        
        //
        application.registerForRemoteNotifications()
        //application.applicationIconBadgeNumber = 1
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}

