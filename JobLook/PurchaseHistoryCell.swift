//
//  PurchaseHistoryCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/9/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class PurchaseHistoryCell: UICollectionViewCell {
    
    var experiment: Experiment? {
        didSet {
            
            titleLabel.text = experiment?.title
        }
    }
    
    
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tour Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    let arrowlabel: UILabel = {
        let label = UILabel()
        label.text = ">"
        label.font = UIFont.systemFont(ofSize: 14)
       label.textColor = UIColor.lightGray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .yellow
        
        setupView()
    }
    
    func setupView() {
        addSubview(titleLabel)
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(arrowlabel)
        arrowlabel.anchor(top: nil, left: nil, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 5, height: 0, width: 0)
        arrowlabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
