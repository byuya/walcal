//
//  UserProfilePhotoCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/24.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class UserProfilePhotoCell: UICollectionViewCell {
    
    var post: Post? {
        didSet{
            guard let imageUrl = post?.imageUrl else {return}

            photoImageView.loadImage(urlString: imageUrl)
            
            categoryLabel.text = post?.category
          
            placeLabel.text = post?.place
           
            titleLabel.text = post?.title
           
            guard let price = post?.price else {return}
            let priceInt = String(price)
            priceLabel.text = "¥\(priceInt)"
            
//            guard let intCount = post?.read else {return}
//            let stringCount = String(intCount)
//            badgeLabel.text = stringCount
        }
    }
    
        let photoImageView: CustomImageView = {
            let iv = CustomImageView()
            iv.contentMode = .scaleAspectFill
            iv.clipsToBounds = true
            return iv
        
        }()
    
//    let badgeLabel: UILabel = {
//        let label = UILabel()
//        label.textColor = UIColor(white: 0, alpha: 0.5)
//        label.backgroundColor = .red
//        label.font = UIFont.boldSystemFont(ofSize: 12)
//        label.text = "2"
//        label.textAlignment = .center
//        label.textColor = .white
//        return label
//    }()
    
    let categoryLabel: UILabel = {
        let cl = UILabel()
        cl.text = "category"
        cl.font = UIFont.systemFont(ofSize: 10)
        cl.textColor = UIColor.lightGray
        cl.textAlignment = .left
        return cl
    
    }()
    
    let placeLabel: UILabel = {
        let pl = UILabel()
        pl.text = "Shibuya"
        pl.font = UIFont.systemFont(ofSize: 10)
        pl.textColor = UIColor.lightGray
        pl.textAlignment = .left
        return pl
    }()
    
    let titleLabel: UILabel = {
        let tl = UILabel()
        tl.text = "Title"
        tl.font = UIFont.boldSystemFont(ofSize: 13)
        tl.textColor = .black
        tl.textAlignment = .left
        return tl
        
    }()
    
    let priceLabel: UILabel = {
        let pl = UILabel()
        pl.text = "1人あたり: ¥5,000"
        pl.font = UIFont.systemFont(ofSize: 10)
        pl.textColor = .black
        pl.textAlignment = .left
        return pl
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(photoImageView)
        photoImageView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
//        addSubview(badgeLabel)
//        badgeLabel.anchor(top: photoImageView.topAnchor, left: nil, bottom: nil, right: photoImageView.rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, height: 20, width: 20)
//        badgeLabel.layer.cornerRadius = 20 / 2
//        badgeLabel.layer.masksToBounds = true
        
        addSubview(categoryLabel)
        categoryLabel.anchor(top: photoImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
        addSubview(placeLabel)
        placeLabel.anchor(top: categoryLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
        addSubview(titleLabel)
        titleLabel.anchor(top: placeLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
        addSubview(priceLabel)
        priceLabel.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
