//
//  User.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/25.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import Foundation

//avoid duplication of fetching user data part2
struct User {
    
    var id: String?
    let uid: String
    let username: String
    let profileImageUrl: String
    let currency: String
    
    init(uid: String, dictionary: [String: Any]) {
        self.uid = uid
        self.username = dictionary["username"] as? String ?? ""
        self.profileImageUrl = dictionary["profileImagerl"] as? String ?? ""
        self.currency = dictionary["currency"] as? String ?? ""
    }
}


