//
//  EditProfileCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/4/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

class EditProfileCell: UICollectionViewCell {
    
    var fund: Fund? {
        didSet {
            
            titleLabel.text = fund?.title
            guestNameLabel.text = fund?.guestName
            
            guard let price = fund?.amount else {return}
            priceLabel.text = String(price)
            
        
        }
    }
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tokyo Tour "
        label.font = UIFont.boldSystemFont(ofSize: 20)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.text = "12/31/2018"
        label.font = UIFont.systemFont(ofSize: 15)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let guestNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Guest1"
        label.font = UIFont.systemFont(ofSize: 15)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.text = "¥1,000 "
        label.font = UIFont.boldSystemFont(ofSize: 30)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .blue
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(titleLabel)
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(dateLabel)
        dateLabel.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(guestNameLabel)
        guestNameLabel.anchor(top: dateLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(priceLabel)
        priceLabel.anchor(top: topAnchor, left: titleLabel.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView)
        seperatorView.anchor(top: guestNameLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
