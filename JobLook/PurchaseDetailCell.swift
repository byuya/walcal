//
//  PurchaseDetailCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/9/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

protocol PurchaseDetailCellDelegate {
    func didTapFeedBack()
    func didTapCancel(for cell: PurchaseDetailCell)
    func didTapCancelRequest()
}
class PurchaseDetailCell: UICollectionViewCell {
    
    var delegate: PurchaseDetailCellDelegate?
    
    var experiment: Experiment? {
        didSet{
            
            titleLabel.text = experiment?.title
            
            //guard let time = experiment?.expireDate.timeIntervalSince1970 else {return}
            
//            if time < Date().timeIntervalSince1970 {
//                cancelSendButton.backgroundColor = .gray
//                cancelSendButton.isEnabled = false
//            }
            
            guard let cancel = experiment?.cancelRequest else {return}
            //cancel requested by guest
            
            if cancel == 0 {
                cancelSendButton.addTarget(self, action: #selector(sendCancelRequest), for: .touchUpInside)

            //state that guest sent request
            } else if cancel == 1 {
                cancelSendButton.setTitle("Cancel Request Sent", for: .normal)
                cancelSendButton.isEnabled = false
                cancelSendButton.backgroundColor = UIColor.purple
                giveFeedBackButton.isEnabled = false
                giveFeedBackButton.backgroundColor = UIColor.lightGray

            //cancel requested by host
            } else if cancel == 2 {
                cancelSendButton.setTitle("Cancelled by Host", for: .normal)
                cancelSendButton.isEnabled = false
                //cancelSendButton.addTarget(self, action: #selector(sendCancel), for: .touchUpInside)
            }
            
        }
    }
    
    let feedbackLabel: UILabel = {
        let label = UILabel()
        label.text = "About This Experience"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tour Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    lazy var giveFeedBackButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Submit Feedback", for: .normal)
        button.backgroundColor = UIColor.mainBlue()
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleGiveFeedBack), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleGiveFeedBack() {
        delegate?.didTapFeedBack()
        
    }
    
    lazy var cancelSendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        button.backgroundColor = UIColor.cyan
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        //button.addTarget(self, action: #selector(sendCancel), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func sendCancel() {
        print("handling cancel button")
        delegate?.didTapCancel(for: self)
    }
    
    @objc fileprivate func sendCancelRequest() {
        print("sending cancel request by guest")
        delegate?.didTapCancelRequest()
        //cancelSendButton.setTitle("Cancel Request Sent", for: .normal)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .yellow
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(feedbackLabel)
        feedbackLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 40, width: 0)
        
        addSubview(titleLabel)
        titleLabel.anchor(top: feedbackLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(giveFeedBackButton)
        giveFeedBackButton.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 50, width: 0)
        
        addSubview(cancelSendButton)
        cancelSendButton.anchor(top: giveFeedBackButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 50, width: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
