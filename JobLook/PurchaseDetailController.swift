//
//  PurchaseDetailController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/9/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class PurchaseDetailController: UICollectionViewController, UICollectionViewDelegateFlowLayout, PurchaseDetailCellDelegate {
    
    let cellId = "cellId"
    
    var experiment: Experiment?
    //var chargeId: String?
    //var amount: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView.register(PurchaseDetailCell.self, forCellWithReuseIdentifier: cellId)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleBackToRoot), name: SendFeedbackController.backToRootName, object: nil)
        
        print(experiment?.title ?? "", experiment?.accountId ?? "", experiment?.chargeId ?? "", experiment?.id ?? "", experiment?.hostId ?? "", experiment?.expireDate ?? "")
        
        //navigationItem.rightBarButtonItem = UIBarButtonItem(title: "window", style: .plain, target: self, action: #selector(handleWindow))
        
        //NotificationCenter.default.addObserver(self, selector: #selector(handleCancelRequest), name: Launcher.callCancelRequest, object: nil)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(handleAcceptCancel), name: CancelAcceptByGuestContorller.acceptCancel, object: nil)
        
        //fetchChargeId()
        
    }
    
    @objc fileprivate func handleBackToRoot() {
        print("coming from send feedback controller")
        navigationController?.popToRootViewController(animated: true)
    }
    
//    func fetchChargeId() {
//
//        print("what the hell")
//
//        guard let userId = Auth.auth().currentUser?.uid else {return}
//        //guard let userId = experiment?.user.uid else {return}
//        guard let hostId = experiment?.hostId else {return}
//        guard let experimentId = experiment?.id else {return}
//
//        print("printing", userId, hostId, experimentId)
//
//        let ref = Database.database().reference().child("experiment").child(userId).child(hostId).child(experimentId)
//        ref.observe(.value, with: { (snapshot) in
//            //print("snapshot value here", snapshot.value)
//
//            guard let dictionary = snapshot.value as? [String: Any] else {return}
//            dictionary.forEach({ (key, value) in
//                if key == "amount" {
//                    self.amount = value as? Int
//                    print("this is amount for the event", self.amount ?? "")
//                }
//                if key == "id" {
//                    self.chargeId = value as? String
//                    print("this is chargeId", self.chargeId ?? "")
//                }
//            })
//
//        }) { (err) in
//            print("failed to fetch data")
//        }
//
//    }
    
    func processRefund() {
            
        guard let uid = Auth.auth().currentUser?.uid else {return}
        //guard let chargeId = experiment?.chargeId else {return}
        guard let chargeId = experiment?.chargeId else {return}
        
        print("chargeId", chargeId)
        
            let value = ["charge": chargeId]
            let cancelRef = Database.database().reference().child("users").child(uid).child("refund")
            cancelRef.childByAutoId().updateChildValues(value) { (err, ref) in
                if let err = err {
                    print("failed to insert refund data", err)
                    return
                }
                
                print("successfully insered refund data")
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                    deleteExperimentData()
                })
                
        }
    
        
    func deleteExperimentData() {
            
            //start deleting after refund
            guard let uid = Auth.auth().currentUser?.uid else {return}
            //guard let postId = self.postId else {return}
            guard let hostId = self.experiment?.hostId else {return}
            guard let experimentId = self.experiment?.id else {return}
            let deleteRef = Database.database().reference().child("experiment").child(uid).child(hostId).child(experimentId)
            deleteRef.removeValue(completionBlock: { (err, ref) in
                if let err = err {
                    print("failed to delete experiment data", err)
                    return
                }
                
                print("Successfully deleted experiment data")
                
                self.dismiss(animated: true, completion: nil)
                
            })
        }
        
    }
    
    func didTapFeedBack() {
        print("coming from detail delegate")
        
        let sendFeedbackController = SendFeedbackController(collectionViewLayout: UICollectionViewFlowLayout())
        sendFeedbackController.experiment = experiment
        navigationController?.present(sendFeedbackController, animated: true, completion: nil)
    
    }
    
    let purchaseHistoryController = PurchaseHistoryController()
    
    //this is under Launcer.swift in util file
    //let launcher = Launcher()
    
    func didTapCancelRequest() {
        print("Requesting cancel request by guest")
        
        let alertController = UIAlertController(title: "Are you sure to send cancel request?", message: "By cliking 'Accept', a cancel request will be sent", preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Accept", style: .destructive, handler: { (_) in
            do {
                try self.sendingCancelRequest()
            } catch let error {
                print("failed to process through alertaction", error)
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
        //launcher.handleWindow()
        
    }
    
    func sendingCancelRequest() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let hostId = experiment?.hostId else {return}
        guard let experimentId = self.experiment?.id else {return}
        
        print("this is data", uid, hostId, experimentId)
        
        let ref = Database.database().reference().child("experiment").child(uid).child(hostId).child(experimentId).child("metadata")
        ref.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
            if var cancelRequestData = currentData.value as? [String: Any] {
                var cancelRequest = cancelRequestData["cancelRequest"] as! Int
                
                print("make sure what this is", cancelRequest)
                
                cancelRequest = 1
                cancelRequestData["cancelRequest"] = cancelRequest
                currentData.value = cancelRequestData
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }
        
        //self.navigationController?.popToRootViewController(animated: true)
        //self.navigationController?.popViewController(animated: true)
        
    }
    
    //let cancelAcceptByGuestController = CancelAcceptByGuestContorller()
    
    func didTapCancel(for cell: PurchaseDetailCell) {
        //cancelAcceptByGuestController.handleWindow()
        
        print("coming from cancel delegate")
        processRefund()
        //launcher.handleWindow()
        
        //guard let indexPath = collectionView.indexPath(for: cell) else {return}
//
//        let experiment = self.experiment
//
//        let cancelViewController = CancelViewController(collectionViewLayout: UICollectionViewFlowLayout())
//        cancelViewController.experiment = experiment
//        navigationController?.present(cancelViewController, animated: true, completion: nil)
//
    }
    
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PurchaseDetailCell
        cell.experiment = self.experiment
        cell.delegate = self
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 250)
    }
    
    var indicatorActivity: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func startLoadingAnimation() {
        
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        collectionView.addSubview(view)
        view.anchor(top: nil, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 110, paddingBottom: 0, paddingRight: 110, height: 80, width: 60)
        view.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        
        view.addSubview(indicatorActivity)
        indicatorActivity.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        //indicatorActivity.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //indicatorActivity.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        indicatorActivity.hidesWhenStopped = true
        indicatorActivity.style = UIActivityIndicatorView.Style.whiteLarge
        
        indicatorActivity.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoadingAnimation() {
        indicatorActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func popupViewWindow() -> UIViewController {
        
        let popupController = UIViewController()
        
        let view = UIView()
        view.backgroundColor = .gray
       
        //popupView.backgroundColor = UIColor.mainBlue()
        
        let thanksLabel: UILabel = {
            let label = UILabel()
            label.text = "Cancel Request Sent! :) \n Please wait for a response from the host"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textColor = .white
            label.numberOfLines = 0
            label.textAlignment = .center
            return label
        }()
        
        let okButton: UIButton = {
            let button = UIButton()
            button.setTitle("Ok", for: .normal)
            button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
            return button
        }()
        
//        let backButton: UIButton = {
//            let button = UIButton()
//            button.setTitle("Back", for: .normal)
//            button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
//            return button
//        }()
        
        collectionView.addSubview(view)
        view.anchor(top: nil, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 180, width: 100)
        view.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 10
        
        view.addSubview(thanksLabel)
        thanksLabel.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 30, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
        
        let seperatorView2 = UIView()
        seperatorView2.backgroundColor = UIColor.white
        view.addSubview(seperatorView2)
        seperatorView2.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 50, paddingRight: 10, height: 1.0, width: 0)
        
        view.addSubview(okButton)
        okButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, height: 0, width: 0)
        okButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        return popupController
        
    }
    
    @objc fileprivate func handleBack() {
        print("back to rootview")
        self.navigationController?.popToRootViewController(animated: true)
        //self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc fileprivate func handleCancel() {
        print("cancel")
        
    }
    
    //    @objc func handleCancelRequest() {
    //
    //        print("function is called", experiment?.title ?? "")
    //
    //        sendingCancelRequest()
    //        startLoadingAnimation()
    //
    //        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
    //            //calling cancelRequest function in PurchaseDetailController
    //            self.popupViewWindow()
    //            self.stopLoadingAnimation()
    //
    //        })
    //    }
    
    //    @objc func handleAcceptCancel() {
    //        print("handling accepting cancel request")
    //
    //        startLoadingAnimation()
    //        processRefund()
    //
    //        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
    //            //calling cancelRequest function in PurchaseDetailController
    //            self.popupViewWindow()
    //            self.stopLoadingAnimation()
    //
    //        })
    //
    //    }
    
}
