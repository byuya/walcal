//
//  DataManager.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/15.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class DateManager: NSObject {
    
    var currentMonthsDates = [NSDate]()
    var selectedDate = NSDate()
    let daysPerWeek: Int = 7
    var numberOfItems: Int!


func daysAcquisition() -> Int {
    let rangeOfWeeks = Calendar.current.range(of: .weekOfMonth, in: .month, for: firstDateMonth() as Date)
    
    let numberOfWeeks = Int((rangeOfWeeks?.count)!)
    numberOfItems = numberOfWeeks * daysPerWeek
    return numberOfItems
}
    
    func firstDateMonth() -> Date {
        
        var components = Calendar.current.dateComponents([.year, .month, .day], from: selectedDate as Date)
        
        components.day = 1
        let firstDateMonth = Calendar.current.date(from: components)!
        return firstDateMonth
        
    }
    
    

}
