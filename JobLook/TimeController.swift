//
//  TimeController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/26.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class TimeController: UICollectionViewController {
    
    var timeArray = ["First", "Second", "Third", "Forth" ]
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.text = "TimeDatePicker"
        tv.backgroundColor = .lightGray
        tv.isEditable = false
        return tv
    }()
    
    lazy var timePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        picker.minuteInterval = 10
        textView.inputView = picker
        //picker.minuteInterval = 30
        picker.addTarget(self, action: #selector(handleSelectedTime), for: .valueChanged)
        return picker
    }()
    
    @objc func handleSelectedTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let selectTime = dateFormatter.string(from: timePicker.date)
        
        textView.inputView = timePicker
        textView.text = selectTime
        
        print(selectTime)
        
        return selectTime
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .cyan
        view.addSubview(textView)
        textView.anchor(top: view.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: view.frame.width)
    }
    
    
}
