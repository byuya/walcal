//
//  Host.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/23/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import Foundation

struct Host {

    let user: User
    let id: String?

    init(user: User, dictionary: [String: Any]) {
        self.user = user
        self.id = dictionary["id"] as? String ?? ""

    }
}
