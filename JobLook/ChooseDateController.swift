//
//  ChooseDateController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/09/02.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FSCalendar
import CalculateCalendarLogic

class ChooseDateController: UICollectionViewController, UICollectionViewDelegateFlowLayout, FSCalendarDelegate, FSCalendarDataSource, ChooseDateCellDelegate, ChooseDateHeaderDelegate {
    
    func didTapChoose(for: ChooseDateCell) {
        print("coming from didTapChoose")
        
        let paymentViewController = PaymentViewController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(paymentViewController, animated: true)
    }
    
    //let chooseDateHeader = ChooseDateHeader()
    let cellId = "cellId"
    let headerId = "headerId"
    
//    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
//        <#code#>
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //fsCalender.delegate = self
        //fsCalender.dataSource = self
        
        collectionView.backgroundColor = .brown
        collectionView.alwaysBounceVertical = true
        
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        
        collectionView.register(ChooseDateCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(ChooseDateHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        
        fetchDates()
        
    }
    
    var schedules = [Schedule]()
    
    fileprivate func fetchDates() {
        print("fetchin dates for this event")
        
        
        let date = Date()
        
        let tmpDate = Calendar(identifier: .gregorian)
        let year = tmpDate.component(.year, from: date)
        let month = tmpDate.component(.month, from: date)
        let day = tmpDate.component(.day, from: date)
        //let hour = tmpDate.component(.hour, from: date)
        //let minute = tmpDate.component(.minute, from: date)
        let y = String(format: "%02d", year)
        let m = String(format: "%02d", month)
        let d = String(format: "%02d", day)
        //let h = String(format: "%02d", hour)
        //let mi = String(format: "%02d", minute)
        
        print(y, m, d)
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let ref = Database.database().reference().child("schedule")
        ref.observe(.childAdded, with: { (snapshot) in
            
            //print(snapshot.key)
            
            let userId = snapshot.key
            
            let scheduleRef = Database.database().reference().child("schedule").child(userId).child(y).child(m).child(d)
            scheduleRef.observe(.childAdded, with: { (snapshot) in
                            
                guard let dictionary = snapshot.value as? [String: Any] else {return}
            
                Database.fetchUserWithUID(uid: uid, completion: { (user) in
                    let schedule = Schedule(user: user, dictionary: dictionary)
                    
                    let date = schedule.time
                    self.schedules.append(schedule)
                    self.collectionView.reloadData()
                    print(date)
                    
                })
                
            }, withCancel: { (err) in
                print("failed to fetch schedule")
                
            })
        }) { (err) in
            print("failed to fetch userId")
        }
        
    }
    
    var header: ChooseDateHeader?

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! ChooseDateHeader
        //header.schedule = schedules[indexPath.item]
        header.delegate = self
        return header
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 300, height: 360)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChooseDateCell
       
        //cell.schedule = schedules[indexPath.item]
        
        cell.delegate = self
        return cell
    }
    
  
    
    func didSelectDate(for cell: ChooseDateCell) {
       print("is this working?")
        guard let indexPath =  collectionView.indexPath(for: cell) else {return}
        
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChooseDateCell
        let schedule = schedules[indexPath.item]
        print(schedule.time)
        
        let date = Date()
        
        let tmpDate = Calendar(identifier: .gregorian)
        let year = tmpDate.component(.year, from: date)
        let month = tmpDate.component(.month, from: date)
        let day = tmpDate.component(.day, from: date)
        let hour = tmpDate.component(.hour, from: date)
        let minute = tmpDate.component(.minute, from: date)
        let y = String(format: "%02d", year)
        let m = String(format: "%02d", month)
        let d = String(format: "%02d", day)
        let h = String(format: "%02d", hour)
        let mi = String(format: "%02d", minute)
        
        //        titleLabel.text = "Schedule List"
        //        titleLabel.backgroundColor = UIColor.orange
        //
        //        scheduleLabel.text = "NO Schudule"
        //        scheduleLabel.backgroundColor = UIColor.magenta
        
        let da = "\(year)/\(m)/\(d) \(h):\(mi)"
        print(da)
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let ref = Database.database().reference().child("schedule")
        ref.observe(.childAdded, with: { (snapshot) in
            
            //print(snapshot.key)
            
            let userId = snapshot.key
            
            let scheduleRef = Database.database().reference().child("schedule").child(userId).child(y).child(m).child(d)
            scheduleRef.observe(.childAdded, with: { (snapshot) in
                
                //print(snapshot.value)
                
                guard let dictionary = snapshot.value as? [String: Any] else {return}
                
                Database.fetchUserWithUID(uid: uid, completion: { (user) in
                    let schedule = Schedule(user: user, dictionary: dictionary)
                   
                    self.schedules[indexPath.item] = schedule
                    self.collectionView.reloadItems(at: [indexPath])
                    
                })
                
            }, withCancel: { (err) in
                print("failed to fetch schedule")
                
            })
        }) { (err) in
            print("failed to fetch userId")
        }

        //cell.schedule = schedules[indexPath.item]
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return  schedules.count
        return 3
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        
    }
}
