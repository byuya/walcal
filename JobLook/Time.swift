//
//  Time.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/27.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import Foundation

struct Time {
    
    let timeText: String
    let selectTime: Date
}

    func selectedTime() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let selectTime = dateFormatter.string(from: Date())
        
        print(selectTime)
        //textView.text = selectTime

        return selectTime
    }
