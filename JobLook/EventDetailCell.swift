//
//  EventDetailCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/8/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

protocol EvenDetailCellDelegate {
    func didTapHostCancel()
    func didTapAcceptCancel()
    func didTapCancelRequest()
    func didTapMessage()
}
class EventDetailCell: UICollectionViewCell {
    
    var delegate: EvenDetailCellDelegate?
    
    var experiment: Experiment? {
        didSet {
            
            titleLabel.text = experiment?.title
            guestNameLabel.text = experiment?.guestName
        
            guard let cancelRequest = experiment?.cancelRequest else {return}
            
            if cancelRequest == 0 {
               cancelSendButton.addTarget(self, action: #selector(sendRequest), for: .touchUpInside)
                cancelMessageLabel.isHidden = true
                
            } else if cancelRequest == 1 {
                cancelSendButton.setTitle("Click here to accept", for: .normal)
                //cancelSendButton.isEnabled = false
                cancelSendButton.backgroundColor = UIColor.magenta
                cancelSendButton.addTarget(self, action: #selector(handleAcceptCancel), for: .touchUpInside)
                
            } else if cancelRequest == 2 {
                cancelSendButton.setTitle("This event has been calceled", for: .normal)
                cancelSendButton.isEnabled = false
                //cancelSendButton.addTarget(self, action: #selector(sendCancel), for: .touchUpInside)
                
            }
        }
    }
    
    let feedbackLabel: UILabel = {
        let label = UILabel()
        label.text = "Your Event Info"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textColor = UIColor.lightGray
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tour Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    let guestNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Guest Name"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    lazy var startMessageButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Start Message", for: .normal)
        button.backgroundColor = .brown
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleMessage), for: .touchUpInside)
        return button
    }()
    
    @objc func handleMessage() {
        delegate?.didTapMessage()
    }
    
    lazy var cancelSendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        button.backgroundColor = UIColor.magenta
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        //button.addTarget(self, action: #selector(sendCancel), for: .touchUpInside)
        return button
    }()
    
    let cancelMessageLabel: UILabel = {
        let label = UILabel()
        label.text = "※ Cancel request has been sent by guest"
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    
    @objc fileprivate func sendCancel() {
        delegate?.didTapHostCancel()
        
    }
    
    @objc fileprivate func handleAcceptCancel() {
        delegate?.didTapAcceptCancel()
    }
    
    @objc fileprivate func sendRequest() {
        cancelSendButton.setTitle("Cancel Request Sent", for: .normal)
        delegate?.didTapCancelRequest()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.orange
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(feedbackLabel)
        feedbackLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 40, width: 0)
        
        addSubview(titleLabel)
        titleLabel.anchor(top: feedbackLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(guestNameLabel)
        guestNameLabel.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(startMessageButton)
        startMessageButton.anchor(top: guestNameLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 50, width: 0)
        
        addSubview(cancelSendButton)
        cancelSendButton.anchor(top: startMessageButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 50, width: 0)
        
        addSubview(cancelMessageLabel)
        cancelMessageLabel.anchor(top: cancelSendButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
