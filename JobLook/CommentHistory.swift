//
//  CommentHistory.swift
//  JobLook
//
//  Created by Akiya Ozawa on 1/30/31 H.
//  Copyright © 31 Heisei Akiya Ozawa. All rights reserved.
//

import Foundation

struct CommentHistory {
    
    let user: User
    //let post: Post
    let testPost: TestPost
    
    var postUser: String?
    var toId: String?
    var id: String?
    
    
    //let uid: String
    let title: String
    let read: Int
    let creationDate: Date
    
    init(user: User, testPost: TestPost, dictionary:[String: Any]) {
        self.user = user
        //self.uid = uid
        self.testPost = testPost
        self.title = dictionary["title"] as? String ?? ""
        //self.uid = dictionary["uid"] as? String ?? ""
        self.read = dictionary["read"] as? Int ?? 0
        //self.postUser = dictionary["postUser"] as? String ?? ""
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
    }
}
