//
//  SendFeedbackController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/27/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Stripe
import Firebase
import FirebaseAuth
import FirebaseDatabase

class SendFeedbackController: UICollectionViewController, UICollectionViewDelegateFlowLayout, SendFeedbackCellDelegate {
    
    let cellId = "cellId"
    //experimentId = id of childautoid created under experiment node
    var experimentId: String?
    
    //postId = userid for post
    var postId: String?
    //var chargeId: String?
    //var amount: Int?
    
    var experiment: Experiment?
    var post: Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView.register(SendFeedbackCell.self, forCellWithReuseIdentifier: cellId)
        
        print("This is chargeId", experiment?.chargeId ?? "", "this is amount too:", experiment?.amount ?? "", "this is hostId:", experiment?.hostId ?? "", "this is guestName", experiment?.user.username ?? "", "experimentId:", experiment?.id ?? "")
    }
    
    //static let backToRootName = NSNotification.Name(rawValue: "BackToRoot")
    
    func didTapBackToRoot() {
        
        //NotificationCenter.default.post(name: SendFeedbackController.backToRootName, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SendFeedbackCell
        cell.experiment = self.experiment
        cell.delegate = self
        
        return cell
    }
    
    func didTapDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTapFeedback() {
        print("coming from sendfeedbackCell")
        startLoadingAnimation()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change to desired number of seconds
            self.processingFeedBack()
            self.stopLoadingAnimation()
            self.popupViewWindow()
        }
        
//        UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            self.collectionView.backgroundColor?.withAlphaComponent(0.5)
//            self.popupViewWindow()
//        }, completion: nil)
      
    }
    
    func processingFeedBack() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let destination = experiment?.accountId else {return}
        guard let experimentId = experiment?.id else {return}
        //guard let source_transaction = experiment?.chargeId else {return}
        guard let source_transaction = experiment?.chargeId else {return} //this is chargeid for the event
        guard let guestName = experiment?.user.username else {return}
        guard let title = experiment?.title else {return}
        
        let transferDate = Date().timeIntervalSince1970
        //let expireDate = Date().addingTimeInterval(100).timeIntervalSince1970
        
        guard let intAmount = experiment?.amount else {return}
        //guard let intAmount = amount else {return}
        //guard let intAmount = (post?.price) else {return}
        let profitAmount = (UInt(Double(intAmount) * 0.9))
        //let amount = (UInt(Double(intAmount) * 0.9))
         guard let hostId = experiment?.hostId else {return}
        
        print(destination, profitAmount, source_transaction)
        
        let values = ["amount": profitAmount, "source_transaction": source_transaction, "destination": destination] as [String: Any]
        let ref = Database.database().reference().child("users").child(uid).child("transfers")
        ref.childByAutoId().updateChildValues(values) { (err, ref) in
            if let err = err {
                print("failed to insert transfer data", err)
                return
            }
            
            //successfully tranfered
            print("successfully inserted transfer data")
            
            let fundValue = ["guestName": guestName, "title": title, "amount": profitAmount, "source_transaction": source_transaction, "destination": destination, "transferDate": transferDate, "payout_process": 0] as [String: Any]
            let ref = Database.database().reference().child("users").child(hostId).child("funds")
            ref.childByAutoId().updateChildValues(fundValue, withCompletionBlock: { (err, ref) in
                if let err = err {
                    print("failed to insert funds data", err)
                    return
                }
                
                print("Successfully inserted fund data")
                
            })
            
            self.saveExperimentHistory(uid: uid, hostId: hostId, experimentId: experimentId)
            
            //guard let experimentId = self.experimentId else {return}
            guard let experimentId = self.experiment?.id else {return}
            //guard let postId = self.postId else {return}
            
            let deleteRef = Database.database().reference().child("experiment").child(uid).child(hostId).child(experimentId)
            deleteRef.removeValue(completionBlock: { (err, ref) in
                if let err = err {
                    print("failed to delete data", err)
                }
                
                //successfully deleted data under experiment node
                print("successfully deleted experiment data")
                
            })
            
        }
    }
    
    func saveExperimentHistory(uid: String, hostId: String, experimentId: String) {
        
        let saveExpRef = Database.database().reference().child(uid).child(hostId).child(experimentId)
        saveExpRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            print("snapshot", snapshot.value)
            
            guard let expDictionary = snapshot.value as? [String: Any] else {return}
            
        let historyRef = Database.database().reference().child(uid)
        
        }) { (err) in
            print("filed to fetch experiment data", err.localizedDescription)
            return
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 300)
    }
    
    var indicatorActivity: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func startLoadingAnimation() {
        
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 10
        
        collectionView.addSubview(view)
        view.anchor(top: nil, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 110, paddingBottom: 0, paddingRight: 110, height: 80, width: 60)
        view.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        
        view.addSubview(indicatorActivity)
        indicatorActivity.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        //indicatorActivity.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //indicatorActivity.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        indicatorActivity.hidesWhenStopped = true
        indicatorActivity.style = UIActivityIndicatorView.Style.whiteLarge
        
        indicatorActivity.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoadingAnimation() {
        indicatorActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    
    func popupViewWindow() {
        
        let popupView = UIView()
        popupView.backgroundColor = UIColor.mainBlue()
        
        let thanksLabel: UILabel = {
            let label = UILabel()
            label.text = "Thank you! \nWe're are looking forward  to seeing you again! :)"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textColor = .white
            label.numberOfLines = 0
            label.textAlignment = .center
            return label
        }()
        
        let okButton: UIButton = {
            let button = UIButton()
            button.setTitle("Ok", for: .normal)
            button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
            return button
        }()
        
        collectionView.addSubview(popupView)
        popupView.anchor(top: nil, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 80, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 180, width: 100)
        popupView.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        popupView.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        popupView.layer.masksToBounds = true
        popupView.layer.cornerRadius = 10
        
        popupView.addSubview(thanksLabel)
        thanksLabel.anchor(top: popupView.topAnchor, left: popupView.leftAnchor, bottom: nil, right: popupView.rightAnchor, paddingTop: 30, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
        
        let seperatorView2 = UIView()
        seperatorView2.backgroundColor = UIColor.white
        popupView.addSubview(seperatorView2)
        seperatorView2.anchor(top: nil, left: popupView.leftAnchor, bottom: popupView.bottomAnchor, right: popupView.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 50, paddingRight: 10, height: 1.0, width: 0)
        
        popupView.addSubview(okButton)
        okButton.anchor(top: nil, left: popupView.leftAnchor, bottom: popupView.bottomAnchor, right: popupView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, height: 0, width: 0)
        okButton.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true
        
    }
    
    static let backToRootName = NSNotification.Name(rawValue: "BackToRoot")
    
    @objc fileprivate func handleBack() {
        print("back to rootview")
        NotificationCenter.default.post(name: SendFeedbackController.backToRootName, object: nil)
        //self.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
}
