//
//  AddCardController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/18.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import UserNotifications
import Stripe
import Firebase
import FirebaseAuth
import FirebaseDatabase

class AddCardController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AddCardHeaderDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    let cellId = "cellId"
    let headerId = "headerId"
    
    var post: Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView.register(AddCardCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView?.register(AddCardHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        
       fetchCard()
    }
    
    var cards = [Card]()
    
    func fetchCard() {
        
        //justAdddView = true
        
        guard let post = self.post else {return}
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let ref = Database.database().reference().child("users").child(uid).child("sources")
        ref.observe(.childAdded, with: { (snapshot) in
            
            guard let dictionary = snapshot.value else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                let card = Card(user: user, post: post, dictionary: dictionary as! [String : Any])
                
                guard let cardId = card.cardId else {return}
                
                print(cardId)
                
                self.cards.append(card)
                
                //self.justAdddView = false
                self.collectionView.reloadData()
                
            })
        }) { (err) in
            print("faled to observe source data")
        }
        
    }
    
    func didTapHeaderDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    var addCardCell: AddCardCell?

    func didTapHeaderHandleAddCard(for cardNumber: String, for expYearNumber: String, for expMonthNumber: String, for cvc: String) {
        
        //Setup STripeToken
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        cardParams.expYear = UInt(expYearNumber)!
        cardParams.expMonth = UInt(expMonthNumber)!
        
        cardParams.cvc = cvc
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                
                guard let errorMessage = error?.localizedDescription else {return}
                
                let alertController = UIAlertController(title: "Sorry:(\n \(errorMessage)", message: "Please try again", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
               
                print("failed to create token", error.debugDescription)
                return
            }
            
            //if successfully created token
            print(token.tokenId)
            
            let value = ["token": token.tokenId]
            
            guard let uid = Auth.auth().currentUser?.uid else {return}
            
            let ref = Database.database().reference().child("users").child(uid).child("sources")
            let childRef = ref.childByAutoId()
            
            childRef.updateChildValues(value, withCompletionBlock: { (err, ref) in
                if let err = err {
                    print("failed to store payment info into dfirebase db", err)
                }
    
                print("successfully stored payment info", childRef.key ?? "")
                self.exists = false
                //removing cards
                self.cards.removeAll()
            
                guard let childIdKey = childRef.key else {return}
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    print("getting this???")
                    self.observeErrorData(uid: uid, childIdKey: childIdKey)
                })
                
                
                //self.dismiss(animated: true, completion: nil)
                //NotificationCenter.default.post(name: AddCardController.addingCardNotificationName, object: nil)
            })
        }
    }
    
     var exists = true
    
    func observeErrorData(uid: String, childIdKey: String) {
        
        let observeChildKey = Database.database().reference().child("users").child(uid).child("sources").child(childIdKey)
        let observeError = observeChildKey.child("error")
       
        observeError.observe(.value, with: { (snaps) in
            
            if snaps.exists() {
                self.exists = true
                print("there is an error", snaps.value)
                guard let errorMessage = snaps.value as? String else {return}
                let alertController = UIAlertController(title: "Sorry:(\n \(errorMessage)", message: "Please try again", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                    self.fetchCard()
                }))
                //alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
                
                observeChildKey.removeValue()
                return
                
            } else if !self.exists {
                print("why not working")
                self.exists = false
                let alertController = UIAlertController(title: "New Card was Added!", message: "Now, you can choose the new card", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
                    self.fetchCard()
                }))
                self.present(alertController, animated: true, completion: nil)
               return
            }
            
        }, withCancel: { (err) in
            print("fatch to observe data", err.localizedDescription)
            return
        })
    }
    
    func fetchCardAfter() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            print("excecuting start")
            self.fetchCard()
            
        })
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! AddCardHeader
        header.delegate = self
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 350)
        
    }
    
    func didTapDismiss() {
        print("coming from dismiss from addcell")
        dismiss(animated: true, completion: nil)
        
    }
   
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! AddCardCell
            cell.card = cards[indexPath.item]
            return cell
    
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cards.count
    }
    
    static let addingHeaderCardNotificationName = NSNotification.Name(rawValue: "addingHeaderCard")
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let card = cards[indexPath.item]
        
        guard let id = card.cardId else {return}
        guard let brand = card.brand else {return}
        guard let last4 = card.last4 else {return}
        
        //print("this is cardid", card.cardId)
        
        dismiss(animated: true, completion: nil)
        
        NotificationCenter.default.post(name: AddCardController.addingHeaderCardNotificationName, object: nil, userInfo: ["id" : id, "brand": brand, "last4": last4])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: view.frame.width, height: 60)
       
        
    }
    
    func didTapCheck(for cell: AddCardCell) {
        print("is this working")
        //justAdddView = true
       self.cards.removeAll()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            //self.justAdddView = false
            print("excecuting start")
            self.fetchCard()
            
        })

    }
    
    
    
    
}
