//
//  FSCalendarHeader.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/17.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.


import UIKit
import Firebase
import FSCalendar
import CalculateCalendarLogic

protocol FSCalendarHeaderDelegate {
    func didSelect(for scheduleHeader: String)

}
class FSCalendarHeader: UICollectionViewCell, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {

    var schedule: Schedule? {
        didSet {
            //let headerLabel = scheduleHeaderLabel.text
        }
    }
    var delegate: FSCalendarHeaderDelegate?

    let scheduleHeaderLabel: UILabel = {
        let label = UILabel()
        label.text = "8:00~9:00 header"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .left
        label.backgroundColor = UIColor.gray
        return label
    }()

    let fsCalender: FSCalendar = {
        let calendar = FSCalendar()
        return calendar
    }()

    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd, HH:mm"
        return formatter
    }()

    // 祝日判定を行い結果を返すメソッド(True:祝日)
    func judgeHoliday(_ date : Date) -> Bool {
        //祝日判定用のカレンダークラスのインスタンス
        let tmpCalendar = Calendar(identifier: .gregorian)

        // 祝日判定を行う日にちの年、月、日を取得
        let year = tmpCalendar.component(.year, from: date)
        let month = tmpCalendar.component(.month, from: date)
        let day = tmpCalendar.component(.day, from: date)

        // CalculateCalendarLogic()：祝日判定のインスタンスの生成
        let holiday = CalculateCalendarLogic()
        return holiday.judgeJapaneseHoliday(year: year, month: month, day: day)

    }

    // date型 -> 年月日をIntで取得
    func getDay(_ date:Date) -> (Int,Int,Int,Int,Int){
        let tmpCalendar = Calendar(identifier: .gregorian)
        let year = tmpCalendar.component(.year, from: date)
        let month = tmpCalendar.component(.month, from: date)
        let day = tmpCalendar.component(.day, from: date)
        let hour = tmpCalendar.component(.hour, from: date)
        let minute = tmpCalendar.component(.minute, from: date)
        return (year,month,day,hour,minute)
    }

    //曜日判定(日曜日:1 〜 土曜日:7)
    func getWeekIdx(_ date: Date) -> Int{
        let tmpCalendar = Calendar(identifier: .gregorian)
        return tmpCalendar.component(.weekday, from: date)
    }

    // 土日や祝日の日の文字色を変える
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        //祝日判定をする（祝日は赤色で表示する）
        if self.judgeHoliday(date){
            return UIColor.red
        }

        //土日の判定を行う（土曜日は青色、日曜日は赤色で表示する）
        let weekday = self.getWeekIdx(date)
        if weekday == 1 {   //日曜日
            return UIColor.red
        }
        else if weekday == 7 {  //土曜日
            return UIColor.blue
        }

        return nil
    }

    let fsViewController = FSCalenderViewController()

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let tmpDate = Calendar(identifier: .gregorian)
        let year = tmpDate.component(.year, from: date)
        let month = tmpDate.component(.month, from: date)
        let day = tmpDate.component(.day, from: date)
        //let hour = tmpDate.component(.hour, from: date)
        //let minute = tmpDate.component(.minute, from: date)
        let y = String(format: "%02d", year)
        let m = String(format: "%02d", month)
        let d = String(format: "%02d", day)
        //let h = String(format: "%02d", hour)
        //let mi = String(format: "%02d", minute)

        //let da = "\(year)/\(m)/\(d)"

        guard let uid = Auth.auth().currentUser?.uid else {return}

        let ref = Database.database().reference().child("schedule").child(uid).child(y).child(m).child(d)
        ref.observeSingleEvent(of: .childAdded, with: { (snapshot) in

            guard let dictionary = snapshot.value as? [String: Any] else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                let schedule = Schedule(user: user, dictionary: dictionary)
                let scheduleHeader = schedule.time

                //self.scheduleHeaderLabel.text = scheduleHeader

                self.delegate?.didSelect(for: scheduleHeader)
            })
        }) { (err) in
            print("failed to schedule date", err)
        }


    }

    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        // fetching from firebase db. if there is a date stored on the date, a dot will be shown
        let tmpDate = Calendar(identifier: .gregorian)
        let year = tmpDate.component(.year, from: date)
        let month = tmpDate.component(.month, from: date)
        let day = tmpDate.component(.day, from: date)
        let m = String(format: "%02d", month)
        let d = String(format: "%02d", day)

        let da = "\(year)/\(m)/\(d)"

        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference().child("schedule").child(uid)
            ref.observe(.childAdded, with: { (snapshot) in
                //print(snapshot.value)

                guard let dictionary = snapshot.value as? [String: Any] else {return}

                Database.fetchUserWithUID(uid: uid, completion: { (user) in
                    let schedule = Schedule(user: user, dictionary: dictionary)

                    if schedule.date == da {
                        print("a dot will show")
                        //return 1
                    }

                })

            }) { (err) in
                print("failed to fetch data")

            }
        }


        return 1

        //        let dateString = self.dateFormatter.string(from: date)
        //
        //        if self.datesWithEvent.contains(dateString) {
        //            print("dot dot")
        //            return 1
        //        }
        //
        //        return 0
        //}
        //        if (timeView.text?.count)! > 0 {
        //            print("is this working??")
        //            return 1
        //
        //        }
        //            return 0
        //
        //    }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        fsCalender.delegate = self
        fsCalender.dataSource = self

        addSubview(fsCalender)
        fsCalender.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 360, width: 300)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
