//
//  CancelViewController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/27/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import Stripe

class CancelViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, CancelViewCellDelegate {
    
    let cellId = "cellId"
    
    var experiment: Experiment?

    var post: Post?
    var postId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView.register(CancelViewCell.self, forCellWithReuseIdentifier: cellId)
        
        print(experiment?.id ?? "")
        print(experiment?.hostId ?? "")
        print(experiment?.chargeId ?? "")
    }
    
    func didTapDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    func didTapCancel() {
        print("coming from cancelviewcell")
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let chargeId = experiment?.chargeId else {return}
        
        print(chargeId)
        
        let value = ["charge": chargeId]
        let cancelRef = Database.database().reference().child("users").child(uid).child("refund")
        cancelRef.childByAutoId().updateChildValues(value) { (err, ref) in
            if let err = err {
                print("failed to insert refund data", err)
                return
            }
            
            print("successfully insered refund data")
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                self.deleteExperimentData()
            })
           
        }
        
    }
    
    fileprivate func deleteExperimentData() {
        
        //start deleting after refund
        guard let uid = Auth.auth().currentUser?.uid else {return}
        //guard let postId = self.postId else {return}
        guard let hostId = self.experiment?.hostId else {return}
        guard let experimentId = self.experiment?.id else {return}
        let deleteRef = Database.database().reference().child("experiment").child(uid).child(hostId).child(experimentId)
        deleteRef.removeValue(completionBlock: { (err, ref) in
            if let err = err {
                print("failed to delete experiment data", err)
                return
            }
            
            print("Successfully deleted experiment data")
            
            self.dismiss(animated: true, completion: nil)
            
        })
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CancelViewCell
        cell.experiment = self.experiment
        cell.delegate = self
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 150)
    }
    
}
