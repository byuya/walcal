//
//  PurchaseHistoryController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/9/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class PurchaseHistoryController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    let headerId = "headerId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView.alwaysBounceVertical = true
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "History", style: .plain, target: self, action: #selector(handleHistory))
        
        collectionView.register(PurchaseHistoryCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView.register(PurchaseHistoryHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        
        fetchExperimentDataByGuest()
    }
    
    @objc func handleHistory() {
        print("handling history")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath)
            as! PurchaseHistoryHeader
        //header.delegate = self
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 70)
        
    }
    
    var experiments = [Experiment]()
    
    func  fetchExperimentDataByGuest() {
        
        //self.experiments.removeAll()
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let experimentIdRef = Database.database().reference().child("experiment-guest").child(uid)
        experimentIdRef.observe(.childAdded, with: { (snapshot) in
            
            let hostId = snapshot.key
            
            //print("snapshot", snapshot.value)
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            dictionary.forEach({ (key, value) in
                //print("this is key", key)
            
        let experimentDataRef = Database.database().reference().child("experiment").child(key)
                experimentDataRef.observeSingleEvent(of: .value, with: { (snap) in
                   // print("this is snapshot", snap.value)
                    
                    guard let expDictionary = snap.value as? [String: Any] else {return}
                    guard let amount = expDictionary["amount"] as? Int else {return}
                    guard let chargeId = expDictionary["id"] as? String else {return}
                    
                    expDictionary.forEach({ (expKey, expValue) in
                        
                        if expKey == "metadata" {
                            
                            guard let metadataDictionary = expValue as? [String: Any] else {return}
                            print(metadataDictionary)
                            
                            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                                var experiment = Experiment(user: user, dictionary: metadataDictionary)
                                experiment.amount = amount
                                
                                experiment.chargeId = chargeId
                                experiment.hostId = hostId
                                experiment.id = key
                                
                                self.experiments.append(experiment)
                                
                                self.collectionView.reloadData()
                            })
                            
                        }
                    })
                }, withCancel: { (err) in
                    print("failed to fetch experimentdata", err.localizedDescription)
                    return
                })
            })
        }) { (err) in
            print("failed to fetch guest data", err.localizedDescription)
            return
        }
    }
    
    
//    fileprivate func fetchEventData() {
//
//        self.experiments.removeAll()
//
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//        let ref = Database.database().reference().child("experiment").child(uid)
//        ref.observe(.value, with: { (snapshot) in
//
//            guard let dictionaries = snapshot.value as? [String: Any] else {return}
//            dictionaries.forEach({ (key, value) in
//
//                self.hostId = key
//
//                let guestRef = Database.database().reference().child("experiment").child(uid).child(key)
//                guestRef.observe(.childAdded, with: { (snapshot) in
//
//                    self.id = snapshot.key
//
//                    guard let dictionary = snapshot.value as? [String: Any] else {return}
//                    Database.fetchUserWithUID(uid: uid, completion: { (user) in
//                        var experiment = Experiment(user: user, dictionary: dictionary)
//                        experiment.id = snapshot.key
//                        experiment.hostId = key
//                        self.experiments.append(experiment)
//                        self.collectionView.reloadData()
//
//                    })
//                }, withCancel: { (err) in
//                    print("failed to fetch")
//                })
//
////                guard let dictionary = value as? [String: Any] else {return}
////
////                Database.fetchUserWithUID(uid: uid, completion: { (user) in
////                    let experiment = Experiment(user: user, dictionary: dictionary)
////
////                    self.experiments.append(experiment)
////
////                    self.collectionView.reloadData()
////            })
//
//            })
//        }) { (err) in
//            print("failed to fetch event data", err)
//        }
//    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PurchaseHistoryCell
        cell.experiment = experiments[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       print("event tapped!")
        
        let experiment = experiments[indexPath.item]
        
        let purchaseDetailController = PurchaseDetailController(collectionViewLayout: UICollectionViewFlowLayout())
        purchaseDetailController.experiment = experiment
        navigationController?.pushViewController(purchaseDetailController, animated: true)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return experiments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //fetchEventData()
        //revisedFetchEventData()
    }
    
//    fileprivate func fetchEventData() {
//
//        self.experiments.removeAll()
//
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//        let ref = Database.database().reference().child("experiment").child(uid)
//        ref.observe(.childAdded, with: { (snapshot) in
//
//            guard let dictionaries = snapshot.value as? [String: Any] else {return}
//            dictionaries.forEach({ (key, value) in
//
//                guard let metadataDictionary = value as? [String: Any] else {return}
//
//                guard let amount = metadataDictionary["amount"] as? Int else {return}
//                guard let chargeId = metadataDictionary["id"] as? String else {return}
//
//                metadataDictionary.forEach({ (metaKey, metaValue) in
//
//                    if metaKey == "metadata" {
//
//                        Database.fetchUserWithUID(uid: uid, completion: { (user) in
//                            var experiment = Experiment(user: user, dictionary: metaValue as! [String : Any])
//
//                            experiment.amount = amount
//                            experiment.chargeId = chargeId
//                            experiment.hostId = snapshot.key
//                            experiment.id = key
//
//                            self.experiments.append(experiment)
//
//                            self.collectionView.reloadData()
//                        })
//                    }
//                })
//
//            })
//        }) { (err) in
//            print("failed to fetch event data", err)
//        }
//    }
    
}
