//
//  PostCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/30.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

protocol PostCellDelegate {
    func didTapGuestButton(post: Post)
}

class PostCell: UICollectionViewCell {
    
    var delegate: PostCellDelegate?
    
    var post: Post? {
        didSet{
            
            guard let imageUrl = post?.imageUrl else {return}
            imageView.loadImage(urlString: imageUrl)
            
            experienceTitleLabel.text = post?.title
            categoryLabel.text = post?.category
            placeLabel.text = post?.place
            languageLabel.text = post?.language
            
            hostTitleLabel.text = "About This Host"
            
            guard let hostImageUrl = post?.user.profileImageUrl else {return}
            hostImageView.loadImage(urlString: hostImageUrl)
            
            hostLabel.text = "Hi, I'm Akiya 26 years old, a Japanese host for 2 years. I will be guiding a city of Shibuya. One of the most crowed place, yet the most excited place."
            
            postTitleLabel.text = "What You Can Experience"
            postDescriptionLabel.text = post?.caption
            
            hostProvideTitleLabel.text = "What Host Provide"
            hostProvideLabel.text = "・Drink fee\n・a guideline book for a way to drink Sake"
            
            participantLabel.text = "Mamimum participants per experience: 5"
            
            guestRequirementTitleLabel.text = "Guest Rewuirement"
            
            
        }
    }
    
    let imageView: CustomImageView = {
        let iv = CustomImageView()
        iv.backgroundColor = .cyan
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
        
    }()
    
    let experienceTitleLabel: UILabel = {
        let cl = UILabel()
        cl.text = "Tokyo Night Tour"
        cl.font = UIFont.boldSystemFont(ofSize: 40)
        cl.textColor = UIColor.white
        cl.textAlignment = .left
        cl.numberOfLines = 0
        return cl
    }()
    
    let categoryLabel: UILabel = {
        let cl = UILabel()
        cl.text = "Category"
        cl.font = UIFont.systemFont(ofSize: 20)
        cl.textColor = UIColor.gray
        cl.textAlignment = .left
        return cl
        
    }()
    
    let placeLabel: UILabel = {
        let pl = UILabel()
        pl.text = "Shibuya"
        pl.font = UIFont.systemFont(ofSize: 20)
        pl.textColor = UIColor.gray
        pl.textAlignment = .left
        return pl
    }()
    
    let languageLabel: UILabel = {
        let ll = UILabel()
        ll.text = "English"
        ll.font = UIFont.systemFont(ofSize: 20)
        ll.textColor = UIColor.gray
        ll.textAlignment = .left
        return ll
    }()
    
    let hostTitleLabel: UILabel = {
        let dl = UILabel()
        dl.text = "About Host"
        dl.textColor = UIColor.black
        //dl.backgroundColor = .brown
        dl.textAlignment = .left
        dl.font = UIFont.boldSystemFont(ofSize: 20)
        dl.numberOfLines = 0
        return dl
    }()
    
    let hostImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.backgroundColor = UIColor.orange
        iv.contentMode = .scaleAspectFill
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 80 / 2
        iv.clipsToBounds = true
        return iv
        
    }()
    
    let hostLabel: UILabel = {
        let hl = UILabel()
        hl.text = "Host Label"
        hl.font = UIFont.systemFont(ofSize: 20)
        hl.textColor = UIColor.gray
        //hl.textAlignment = .left
        hl.numberOfLines = 0
        return hl
    }()
    
    let postTitleLabel: UILabel = {
        let dl = UILabel()
        dl.text = "What You Can Experience"
        dl.font = UIFont.boldSystemFont(ofSize: 20)
        dl.textColor = UIColor.black
        dl.textAlignment = .left
        dl.numberOfLines = 0
        return dl
    }()
    
    let postDescriptionLabel: UILabel = {
        let pl = UILabel()
        pl.text = "This is a post description titile"
        pl.font = UIFont.systemFont(ofSize: 20)
        pl.textColor = UIColor.gray
        pl.textAlignment = .left
        pl.numberOfLines = 0
        return pl
    }()
    
    let hostProvideTitleLabel: UILabel = {
        let pl = UILabel()
        pl.text = "What Host Provide"
        pl.font = UIFont.boldSystemFont(ofSize: 20)
        pl.textColor = UIColor.black
        pl.textAlignment = .left
        pl.numberOfLines = 0
        return pl
    }()
    
    let hostProvideLabel: UILabel = {
        let pl = UILabel()
        pl.text = "・Drink fee\n・a guideline book for a way to drink Sake"
        pl.font = UIFont.systemFont(ofSize: 20)
        pl.textColor = UIColor.gray
        pl.textAlignment = .left
        pl.numberOfLines = 0
        return pl
    }()
    
    let participantLabel: UILabel = {
        let pl = UILabel()
        pl.text = "Mamimum participants per experience: 5"
        pl.font = UIFont.systemFont(ofSize: 20)
        pl.textColor = UIColor.gray
        pl.textAlignment = .left
        pl.numberOfLines = 0
        return pl
    }()
    
    let guestRequirementTitleLabel: UILabel = {
        let gl = UILabel()
        gl.text = "Guest Requirement"
        gl.font = UIFont.boldSystemFont(ofSize: 20)
        gl.textColor = UIColor.black
        gl.textAlignment = .left
        gl.numberOfLines = 0
        return gl
    }()
    
    lazy var guestButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("GUEST REQUIREMENT", for: .normal)
        button.backgroundColor = UIColor.magenta
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(handleGuest), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleGuest() {
        print("this will get you guest req page")
        guard let post = post else {return}
        delegate?.didTapGuestButton(post: post)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //backgroundColor = .green
        //setupCellView()
        
        setupView()
        
    }
    
    func setupView() {
        addSubview(imageView)
        imageView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(experienceTitleLabel)
        experienceTitleLabel.anchor(top: nil, left: imageView.leftAnchor, bottom: imageView.bottomAnchor, right: imageView.rightAnchor, paddingTop: 0, paddingLeft: 5, paddingBottom: 10, paddingRight: 0, height: 0, width: 0)
        
        addSubview(categoryLabel)
        categoryLabel.anchor(top: imageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
        
        addSubview(placeLabel)
        placeLabel.anchor(top: categoryLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
        
        addSubview(languageLabel)
        languageLabel.anchor(top: placeLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView)
        seperatorView.anchor(top: languageLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        addSubview(hostTitleLabel)
        hostTitleLabel.anchor(top: seperatorView.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 80, width: 0)
        
        addSubview(hostImageView)
        hostImageView.anchor(top: seperatorView.bottomAnchor, left: hostTitleLabel.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 20, height: 80, width: 80)
        
        addSubview(hostLabel)
        hostLabel.anchor(top: hostTitleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 100, width: 0)
        
        let seperatorView2 = UIView()
        seperatorView2.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView2)
        seperatorView2.anchor(top: hostLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        addSubview(postTitleLabel)
        postTitleLabel.anchor(top: seperatorView2.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
        
        addSubview(postDescriptionLabel)
        postDescriptionLabel.anchor(top: postTitleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, height: 100, width: 0)
        
        let seperatorView3 = UIView()
        seperatorView3.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView3)
        seperatorView3.anchor(top: postDescriptionLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        addSubview(hostProvideTitleLabel)
        hostProvideTitleLabel.anchor(top: seperatorView3.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 20, width: 0)
        
        addSubview(hostProvideLabel)
        hostProvideLabel.anchor(top: hostProvideTitleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 80, width: 0)
        
        let seperatorView4 = UIView()
        seperatorView4.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView4)
        seperatorView4.anchor(top: hostProvideLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        addSubview(participantLabel)
        participantLabel.anchor(top: seperatorView4.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        let seperatorView5 = UIView()
        seperatorView5.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView5)
        seperatorView5.anchor(top: participantLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        addSubview(guestRequirementTitleLabel)
        guestRequirementTitleLabel.anchor(top: seperatorView5.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
//        addSubview(guestButton)
//        guestButton.anchor(top: guestRequirementTitleLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
    }
    
//    func setupCellView() {
//        //cell 1
//        addSubview(imageView)
//        imageView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 300, width: frame.width)
//        //cell 1
//        addSubview(titleLabel)
//        titleLabel.anchor(top: nil, left: imageView.leftAnchor, bottom: imageView.bottomAnchor, right: imageView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//        //cell 1
//        addSubview(categoryLabel)
//        categoryLabel.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//        //cell 1
//        addSubview(placeLabel)
//        placeLabel.anchor(top: categoryLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//        //cell 1
//        addSubview(languageLabel)
//        languageLabel.anchor(top: placeLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//
//        //cell 2
//        addSubview(hostTitleLabel)
//        hostTitleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//        //cell 2
//        addSubview(hostImageView)
//        hostImageView.anchor(top: topAnchor, left: hostTitleLabel.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 16, paddingLeft: 0, paddingBottom: 0, paddingRight: 16, height: 50, width: 50)
//        hostImageView.layer.cornerRadius = 50 / 2
//        //hostImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        //cell 2
//        addSubview(hostLabel)
//        hostLabel.anchor(top: hostTitleLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//
//        //cell3
//        addSubview(postTitleLabel)
//        postTitleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 24, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//        //cell3
//        addSubview(postDescriptionLabel)
//        postDescriptionLabel.anchor(top: postTitleLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, height: 0, width: 0)
//
//        //cell4
//        addSubview(hostProvideTitleLabel)
//        hostProvideTitleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 24, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//        //cell4
//        addSubview(hostProvideLabel)
//        hostProvideLabel.anchor(top: hostProvideTitleLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//
//        //cell5
//        addSubview(participantLabel)
//        participantLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//
//        //cell6
//        addSubview(guestRequirementTitleLabel)
//        guestRequirementTitleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
