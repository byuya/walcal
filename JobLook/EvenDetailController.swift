//
//  EvenDetailController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/8/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class EvenDetailController: UICollectionViewController, UICollectionViewDelegateFlowLayout, EvenDetailCellDelegate {
    
    let cellId = "cellId"
    
    var window: UIWindow?
    
    var experiment: Experiment?
    var chargeId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView.register(EventDetailCell.self, forCellWithReuseIdentifier: cellId)
     
          //NotificationCenter.default.addObserver(self, selector: #selector(handleCancel), name: CancelWindowByHost.sendingCancelByHost, object: nil)
        
        print("this is experimentid", experiment?.id ?? "", "this is hostId(myId)", experiment?.user.uid ?? "", "this is chargeId", chargeId ?? "", "guestId", experiment?.guestId)
       
        fetchChargeId()
        
        decrementCount()
        decrementHomeBadge()
    }
    
    func decrementCount() {
        
        print("decrement start")
        
        guard let guestId = experiment?.user.uid else {return}
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let experimentId = experiment?.id else {return}
        
        let decrementRef = Database.database().reference().child("experiment").child(guestId).child(uid).child(experimentId).child("metadata")
        decrementRef.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
            if var countData = currentData.value as? [String: Any] {
                var count = countData["read"] as? Int
                
                print("this is count", count)
                
                count = 0
                countData["read"] = count
                currentData.value = countData
                
                return TransactionResult.success(withValue: currentData)
            }
                return TransactionResult.success(withValue: currentData)
        }
    }
    
    func decrementHomeBadge() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let decrementPostRef = Database.database().reference().child("count").child(uid)
        decrementPostRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var currentCountData = currentData.value as? [String: Any] {
                var countData = currentCountData["read"] as! Int
                
                if countData == 0 {
                    return TransactionResult.abort()
                }
                
                print("this is countData", countData)
                
                countData -= 1
                currentCountData["read"] = countData
                currentData.value = currentCountData
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
            
        }) { (err, commited, snapshot) in
            if let err = err {
                print("failed to decrement count", err)
            }
            
            print("this is totalCount", snapshot?.value)
            guard let dictionary = snapshot?.value as? [String: Any] else {return}
            guard let badgeCount = dictionary["read"] as? UInt else {return}
            
            print("badgeCount", badgeCount)
            
            UIApplication.shared.applicationIconBadgeNumber = Int(badgeCount)
            
        }
    }
    
    func fetchChargeId() {
        
        print("what the hell")
        
        guard let userId = Auth.auth().currentUser?.uid else {return}
        //guard let userId = experiment?.user.uid else {return}
        //guard let guestId = experiment?.guestId else {return}
        guard let guestId = experiment?.user.uid else {return}
        guard let experimentId = experiment?.id else {return}
        
        print("printing", userId, guestId, experimentId)
        
        let ref = Database.database().reference().child("experiment").child(guestId).child(userId).child(experimentId)
        ref.observe(.value, with: { (snapshot) in
            //print("snapshot value here", snapshot.value)
            
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            dictionary.forEach({ (key, value) in
                if key == "id" {
                    self.chargeId = value as? String
                    //print("this is chargeId", self.chargeId ?? "")
                }
            })
            
        }) { (err) in
            print("failed to fetch data")
        }
    }
    
    func didTapHostCancel() {
        
        print("coming from eventdetail host cancel")
        
        
    }
    
    func didTapMessage() {
        print("tapping message")
        
        //increaseReadCount()
        guard let userId = experiment?.user.uid else {return}
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.rootViewController = MainTabBarController()
        
        let messageController = MessageController(collectionViewLayout: UICollectionViewFlowLayout())
            messageController.userId = userId
        
        if let mainTabController = window?.rootViewController as? MainTabBarController {
            mainTabController.selectedIndex = 1
            
            //mainTabController.presentedViewController?.dismiss(animated: true, completion: nil)
            
        if let homeNavigationController = mainTabController.viewControllers?[1] as? UINavigationController {
            homeNavigationController.pushViewController(messageController, animated: true)
            
            print("check check 2")
            }
        }
    }
    
    func increaseReadCount() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let userId = experiment?.user.uid else {return}
        
        let guestReadCountRef = Database.database().reference().child("user-messages").child(userId).child(uid)
        guestReadCountRef.updateChildValues(["read": 0]) { (err, ref) in
            if let err = err {
                print("failed to update readcount", err.localizedDescription)
                return
            }
            
            print("Successfullt update readcount to guestId")
            
        let hostReadCountRef = Database.database().reference().child("user-messages").child(uid).child(userId)
            hostReadCountRef.updateChildValues(["read": 0], withCompletionBlock: { (error, reference) in
                if let error = error {
                    print("failed to update readcount of host", error.localizedDescription)
                    return
                }
                
                print("Succefully update readcount of guest")
                
            })

            
        }
    }
    
    func didTapAcceptCancel() {
        print("tapped accepting cancel request by guest")
        
        //do something to show alert saying "Accept cancel by guest" and MAKE REFUND to guest
        let alertController = UIAlertController(title: "Are you sure to accept the request?", message: "By clicking 'Accept', a refund will be made to the guest", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Accept", style: .default, handler: { (_) in
            do {
                
                self.startLoadingAnimation()
                self.processRefundByHost()
                
                //self.makeHistory()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    //self.deleteExperimentData()
                    self.popupViewWindow()
                    self.stopLoadingAnimation()
                    
                })
                
            } catch let err {
                print("failed to show alert", err)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
    //let cancelWindowByHost = CancelWindowByHost()
    
    func didTapCancelRequest() {
        print("cancel requested by host")
        //cancelWindowByHost.handleWindow()
        let alertController = UIAlertController(title: "Are you sure to cancel?", message: "By clicking 'YES', A refund will be made to the guest", preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            do {
                
                //self.changeCancelRequestTo2()
                
                self.startLoadingAnimation()
                self.processRefundByHost()
                
                self.makeHistory()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    self.deleteExperimentData()
                    self.popupViewWindow()
                    self.stopLoadingAnimation()

                    })
                
            } catch let error {
                print("failed to process through alertaction", error)
            }
            
            }))
        
            alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
      
    }
    
//    func changeCancelRequestTo2() {
//
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//        guard let guestId = experiment?.guestId else {return}
//        guard let experimentId = experiment?.id else {return}
//
//        //let ref = Database.database().reference().child("experiment").child(guestId).child(uid).child(experimentId)
//        let cancelRef = ref.child(guestId).child(uid).child(experimentId)
//        cancelRef.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
//            if var cancelRequestData = currentData.value as? [String: Any] {
//                var cancelRequest = cancelRequestData["cancelRequest"] as! Int
//
//                print("make sure what this is", cancelRequest)
//
//                cancelRequest = 2
//                cancelRequestData["cancelRequest"] = cancelRequest
//                currentData.value = cancelRequestData
//                return TransactionResult.success(withValue: currentData)
//            }
//
//            return TransactionResult.success(withValue: currentData)
//        }
//
//        //After making cancelRequest value to 2, refund process will begin.
//        //processRefundByHost()
//    }
    
    func processRefundByHost() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let guestId = experiment?.guestId else {return}
        
        //guard let chargeId = experiment?.chargeId else {return}
        guard let chargeId = chargeId else {return}
        
        print(chargeId)
        
        let value = ["charge": chargeId]
        let cancelRef = Database.database().reference().child("users").child(guestId).child("refund_host").child(uid)
        cancelRef.childByAutoId().updateChildValues(value) { (err, ref) in
            if let err = err {
                print("failed to insert refund data", err)
                return
            }
            
            print("successfully insered refund data")
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            //                self.deleteExperimentData()
            //            })
            
        }
    }
    
    fileprivate func makeHistory() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let guestId = experiment?.guestId else {return}
        guard let experimentId = experiment?.id else {return}
        
        guard let amount = experiment?.price else {return}
        guard let title = experiment?.title else {return}
        guard let guestName = experiment?.guestName else {return}
        guard let chargeId = experiment?.chargeId else {return}
        
        let creationDate = Date().timeIntervalSince1970
        
        let hostvalues = ["amount": amount, "title": title, "guestName": guestName, "creationDate": creationDate] as [String: Any]
        
        let historyHostRef = Database.database().reference().child("history").child(uid).child(guestId).child(experimentId)
        historyHostRef.updateChildValues(hostvalues) { (hostErr, ref) in
            if let hostErr = hostErr {
                print("failed to update hosthistory", hostErr)
                return
            }
            
            print("Successfully inserted host data")
            
            let guestValues = ["amount": amount, "title": title, "hostName": guestName, "creationDate": creationDate, "chargeId": chargeId] as [String: Any]
            let historyGuestRef =  Database.database().reference().child("history").child(guestId).child(uid).child(experimentId)
            historyGuestRef.updateChildValues(guestValues, withCompletionBlock: { (guestErr, ref) in
                if let guestErr = guestErr {
                    print("failed to update guestHistory", guestErr)
                    return
                }
                
                print("Successfully insered guestData")
                
            })
        }
    }
    
    var ref = Database.database().reference().child("experiment")
    
    fileprivate func deleteExperimentData() {
        
        //start deleting after refund
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let guestId = experiment?.guestId else {return}
        //guard let hostId = self.experiment?.hostId else {return}
        guard let experimentId = self.experiment?.id else {return}
        
        let deleteRef = Database.database().reference().child("experiment").child(guestId).child(uid).child(experimentId)
        deleteRef.removeValue(completionBlock: { (err, ref) in
            if let err = err {
                print("failed to delete experiment data", err)
                return
            }
            
            print("Successfully deleted experiment data")
            
            self.dismiss(animated: true, completion: nil)
            
        })
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! EventDetailCell
        cell.experiment = self.experiment
        cell.delegate = self
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 250)
    }
    
    var indicatorActivity: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func startLoadingAnimation() {
        
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        collectionView.addSubview(view)
        view.anchor(top: nil, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 110, paddingBottom: 0, paddingRight: 110, height: 80, width: 60)
        view.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        
        view.addSubview(indicatorActivity)
        indicatorActivity.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        //indicatorActivity.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //indicatorActivity.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        indicatorActivity.hidesWhenStopped = true
        indicatorActivity.style = UIActivityIndicatorView.Style.whiteLarge
        
        indicatorActivity.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoadingAnimation() {
        indicatorActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func popupViewWindow() -> UIViewController {
        
        let popupController = UIViewController()
        
        let view = UIView()
        view.backgroundColor = .gray
        
        //popupView.backgroundColor = UIColor.mainBlue()
        
        let thanksLabel: UILabel = {
            let label = UILabel()
            label.text = "Cancel Request Sent! :) \n Don't worry I got you"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textColor = .white
            label.numberOfLines = 0
            label.textAlignment = .center
            return label
        }()
        
        let okButton: UIButton = {
            let button = UIButton()
            button.setTitle("Ok", for: .normal)
            button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
            return button
        }()
        
        collectionView.addSubview(view)
        view.anchor(top: nil, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 180, width: 100)
        view.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 10
        
        view.addSubview(thanksLabel)
        thanksLabel.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 30, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
        
        let seperatorView2 = UIView()
        seperatorView2.backgroundColor = UIColor.white
        view.addSubview(seperatorView2)
        seperatorView2.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 50, paddingRight: 10, height: 1.0, width: 0)
        
        view.addSubview(okButton)
        okButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, height: 0, width: 0)
        okButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        return popupController
        
    }
    
    @objc fileprivate func handleBack() {
        print("back to rootview")
        self.navigationController?.popToRootViewController(animated: true)
        //self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //ref.removeAllObservers()
        
    }
}
