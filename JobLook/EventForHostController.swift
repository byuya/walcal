//
//  EventForHostController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/6/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import Stripe

class EventForHostController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    
    var chargeId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .red
        
        collectionView.register(EventForHostCell.self, forCellWithReuseIdentifier: cellId)
        
       navigationItem.rightBarButtonItem = UIBarButtonItem(title: "History", style: .plain, target: self, action: #selector(handleHistory))
        
        //fetchEvent()
        //fetchEvent2()
        
        fetchExperimentDataByHost()
       
    }
    
   var indicatorActivity: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func startLoadingAnimation() {
        
        collectionView.addSubview(indicatorActivity)
        indicatorActivity.anchor(top: collectionView.topAnchor, left: collectionView.leftAnchor, bottom: collectionView.bottomAnchor, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        indicatorActivity.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        indicatorActivity.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        
        indicatorActivity.hidesWhenStopped = true
        indicatorActivity.style = UIActivityIndicatorView.Style.whiteLarge
        
        indicatorActivity.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoadingAnimation() {
        indicatorActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    
    @objc func handleHistory() {
        print("handling host history")
        
        startLoadingAnimation()

        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            self.stopLoadingAnimation()
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
            //self.navigationController?.navigationItem.backBarButtonItem?.isEnabled = false
            self.navigationItem.hidesBackButton = true
            self.popupViewWindow()

        })
    
    }
    
    func popupViewWindow() {
        
        let popupView = UIView()
        
        popupView.fadeIn(type: .Normal)
        
        popupView.backgroundColor = .white
        collectionView.addSubview(popupView)
        
        popupView.anchor(top: nil, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 0, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 180, width: 100)
        popupView.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        popupView.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        popupView.layer.masksToBounds = true
        popupView.layer.cornerRadius = 10
        
//        let thanksLabel: UILabel = {
//            let label = UILabel()
//            label.text = "Thank you! \nHave a blast experience! \nLet's start chatting with the host"
//            label.font = UIFont.boldSystemFont(ofSize: 15)
//            label.textColor = .white
//            label.numberOfLines = 0
//            label.textAlignment = .center
//            return label
//        }()
        
        let okButton: UIButton = {
            let button = UIButton()
            button.setTitle("Ok", for: .normal)
            button.setTitleColor(.gray, for: .normal)
            button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
            return button
        }()
        
        popupView.addSubview(okButton)
        okButton.anchor(top: nil, left: popupView.leftAnchor, bottom: popupView.bottomAnchor, right: popupView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, height: 0, width: 0)
        okButton.centerXAnchor.constraint(equalTo: popupView.centerXAnchor).isActive = true
        
    }
    
    @objc func handleBack() {
         //UIApplication.shared.endIgnoringInteractionEvents()
        
         self.navigationController?.popToRootViewController(animated: true)
    }
    
    var experiments = [Experiment]()
    //var countArray = [Int]()
    
    fileprivate func fetchEvent() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let ref = Database.database().reference().child("experiment")
        ref.observe(.value, with: { (snapshot) in
            //this solved duplicating cell views
           
            //self.countArray.removeAll()
            self.experiments.removeAll()
            
        guard let dictionaries = snapshot.value as? [String: Any] else {return}
        dictionaries.forEach({ (key, value) in
                
            let hostRef = Database.database().reference().child("experiment").child(key).child(uid)
            hostRef.observe(.childAdded, with: { (snap) in
                
                guard let hostDictionary = snap.value as? [String: Any] else {return}
                
                hostDictionary.forEach({ (metaKey, metaValue) in
                    
                    if metaKey == "metadata" {
                      // changed to key from uid because experiment belongs to purchaser(guest)
                        Database.fetchUserWithUID(uid: key, completion: { (user) in

                            var experiment = Experiment(user: user, dictionary: metaValue as! [String : Any])
                            //experiment.guestId = key
                            experiment.id = snap.key
                            //experiment.chargeId = self.chargeId ?? ""

                            self.experiments.append(experiment)

                            self.collectionView.reloadData()
                            //var count: Int?
                            
                            //self.countArray.append(count)
//                            let total = self.countArray.reduce(0, {total, number in total + number})
//                            print("this is count", total)
                            
//                            if total == 0 {
//                                self.tabBarController?.tabBar.items?[4].badgeValue = nil
//                            } else {
//                                self.tabBarController?.tabBar.items?[4].badgeValue = "\(total)"
//                            }
                            
                        })
                    }
                })
                    
                }, withCancel: { (err) in
                    print("failed to fetch host data", err)
                })
                
            })
        }) { (err) in
            print("failed to fetch data", err)
        }
        
    }
    
    func  fetchExperimentDataByHost() {
        
        //self.experiments.removeAll()
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let experimentIdRef = Database.database().reference().child("experiment-host").child(uid)
        experimentIdRef.observe(.childAdded, with: { (snapshot) in
            
            let guestId = snapshot.key
            
            //print("snapshot", snapshot.value)
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            dictionary.forEach({ (key, value) in
                //print("this is key", key)
                
                let experimentDataRef = Database.database().reference().child("experiment").child(key)
                experimentDataRef.observeSingleEvent(of: .value, with: { (snap) in
                    // print("this is snapshot", snap.value)
                    
                    guard let expDictionary = snap.value as? [String: Any] else {return}
                    guard let amount = expDictionary["amount"] as? Int else {return}
                    guard let chargeId = expDictionary["id"] as? String else {return}
                    
                    expDictionary.forEach({ (expKey, expValue) in
                        
                        if expKey == "metadata" {
                            
                            guard let metadataDictionary = expValue as? [String: Any] else {return}
                            print(metadataDictionary)
                            
                            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                                var experiment = Experiment(user: user, dictionary: metadataDictionary)
                                experiment.amount = amount
                                experiment.chargeId = chargeId
                                experiment.guestId = guestId
                                experiment.id = key
                                
                                print("hostId", experiment.user.uid, "guestId", experiment.guestId, "chargeId", experiment.chargeId, "expId", experiment.id)
                                
                                self.experiments.append(experiment)
                                
                                self.collectionView.reloadData()
                            })
                            
                        }
                    })
                }, withCancel: { (err) in
                    print("failed to fetch experimentdata", err.localizedDescription)
                    return
                })
            })
        }) { (err) in
            print("failed to fetch guest data", err.localizedDescription)
            return
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! EventForHostCell
        cell.experiment = experiments[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let experiment = experiments[indexPath.item]
        let eventDetailController = EvenDetailController(collectionViewLayout: UICollectionViewFlowLayout())
        eventDetailController.experiment = experiment
        navigationController?.pushViewController(eventDetailController, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return experiments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //fetchEvent()
        //self.experiments.removeAll()
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        self.experiments.removeAll()
//    }
    
    //    func fetchEvent2() {
    //
    //        guard let uid = Auth.auth().currentUser?.uid else {return}
    //
    //        let ref = Database.database().reference().child("experiment")
    //        ref.observe(.childAdded, with: { (snapshot) in
    //
    //            //print("snapshot", snapshot.value)
    //            let keys = snapshot.key
    //
    //        let hostRef = Database.database().reference().child("experiment").child(keys).child(uid)
    //            hostRef.observe(.childAdded, with: { (snap) in
    //
    //                //print("this is host ref snapshot", snap.value)
    //                guard let dictionary = snap.value as? [String: Any] else {return}
    //                dictionary.forEach({ (key, value) in
    //
    //                    print("this is value", value)
    //
    //                    guard let metaDict = value as? [String: Any] else {return}
    //                    metaDict.forEach({ (metaKey, metaValue) in
    //                        if metaKey == "metadata" {
    //
    //                            Database.fetchUserWithUID(uid: keys, completion: { (user) in
    //                                var experiment = Experiment(user: user, dictionary: metaValue as! [String: Any])
    //                                experiment.id = key
    //
    //                                self.experiments.append(experiment)
    //
    //                                self.collectionView.reloadData()
    //                            })
    //                        }
    //                    })
    //                })
    //            }, withCancel: { (err) in
    //                print("failed to fetch hostref", err)
    //            })
    //
    //        }) { (err) in
    //            print("failed to fetch snapshot", err)
    //        }
    //    }
    
}
