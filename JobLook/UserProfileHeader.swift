//
//  UserProfileHeader.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/19.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

protocol UserProfileHeaderDelegate {
    func didChangeToListView()
    func didChangeToGridView()
    func didTapBookmark()
    func didTapEdit()
    func didTapRegister()
    func didTapSend()
    
    
}
class UserProfileHeader: UICollectionViewCell, UICollectionViewDelegateFlowLayout {
    
    var delegate: UserProfileHeaderDelegate?
    
    var user: User? {
        didSet {
            
            guard let profileImageUrl = user?.profileImageUrl else {return}
            profileImageView.loadImage(urlString: profileImageUrl)
            
            usernameLabel.text = user?.username
            
            setupEditProfileButton()
            setupRegisterButton()
        }
        
    }
    
    fileprivate func setupEditProfileButton() {
        
        guard let currentLoggledInUserId = Auth.auth().currentUser?.uid else {return}
        
        guard let userId = user?.uid else {return}
        
        if currentLoggledInUserId == userId {
            
            //edit profile
            handleEditOrFollow()
            editProfileFollowButton.setTitle("Edit", for: .normal)
            //editProfileFollowButton.isEnabled = false
            
        } else {
            
            //cehck if following
            Database.database().reference().child("following").child(currentLoggledInUserId).child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let isFollowing = snapshot.value as? Int, isFollowing == 1 {
                    
                    self.editProfileFollowButton.setTitle("Unfollow", for: .normal)
                } else {
                
                    self.setupFollowStyle()
                }
            }, withCancel: { (err) in
                print("Failed to check if following")
            })
            
        }
        
    }
    
    @objc func handleEditOrFollow() {
        print("execute profile / follow / unfollow logic...")
        
        guard let currentLoggledInUserId = Auth.auth().currentUser?.uid else {return}
        guard let userId = user?.uid else { return }
        
        if currentLoggledInUserId == userId {
            editProfileFollowButton.addTarget(self, action: #selector(handleEdit), for: .touchUpInside)
            
        } else {
        if editProfileFollowButton.titleLabel?.text == "Unfollow" {
            //unfollow
            Database.database().reference().child("following").child(currentLoggledInUserId).child(userId).removeValue(completionBlock: { (err, ref) in
                if let err = err {
                    print("failed to unfollow user", err)
                    return
                }
                
                print("Successfully unfollowed user", self.user?.username ?? "")
                
                self.setupFollowStyle()
                
            })
        } else {
            //follow
            let ref = Database.database().reference().child("following").child(currentLoggledInUserId)
            let values = [userId: 1]
            ref.updateChildValues(values) { (err, ref) in
                if let err = err {
                    print("Failed to follow user", err)
                }
                
                print("Successfully followed user:", self.user?.username ?? "")
                
                self.editProfileFollowButton.setTitle("Unfollow", for: .normal)
                self.editProfileFollowButton.backgroundColor = .white
                self.editProfileFollowButton.setTitleColor(.black, for: .normal)
            }
        }
    }
    
    }

    @objc fileprivate func handleEdit() {
        delegate?.didTapEdit()
        
        }
    
    fileprivate func setupFollowStyle() {
        self.editProfileFollowButton.setTitle("Follow", for: .normal)
        self.editProfileFollowButton.backgroundColor = UIColor.rgb(red: 17, green: 154, blue: 237)
        self.editProfileFollowButton.setTitleColor(.white, for: .normal)
        self.editProfileFollowButton.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
    }
    
    let profileImageView: CustomImageView = {
        let iv = CustomImageView()
        return iv
    }()
    
    lazy var gridButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "grid"), for: .normal)
        button.addTarget(self, action: #selector(handleChangeToGridView), for: .touchUpInside)
        return button
    }()
    
    @objc func handleChangeToGridView() {
        print("changing to grid view")
        gridButton.tintColor = .mainBlue()
        listButton.tintColor = UIColor(white: 0, alpha: 0.2)
        delegate?.didChangeToGridView()
    }
    
    lazy var listButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        button.addTarget(self, action: #selector(handleChangeToListButton), for: .touchUpInside)
        return button
    }()
    
    @objc func handleChangeToListButton() {
        print("Changing to list view")
        listButton.tintColor = .mainBlue()
        gridButton.tintColor = UIColor(white: 0, alpha: 0.2)
        delegate?.didChangeToListView()
        
    }
    
    let badgeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 0.5)
        label.backgroundColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        //label.text = "1"
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    lazy var bookmarkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "ribbon"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        button.addTarget(self, action: #selector(handleBookmark), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleBookmark() {
        delegate?.didTapBookmark()
    }
    
    lazy var sendmarkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "send2"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        button.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        return button
        
    }()
    
    @objc fileprivate func handleSend() {
        print("handling message")
        delegate?.didTapSend()
    }
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "username"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let postLabel: UILabel = {
        let label = UILabel()
        let atttributedText = NSMutableAttributedString(string: "11\n", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
        atttributedText.append(NSAttributedString(string: "posts", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)]))
        label.attributedText = atttributedText
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let followersLabel: UILabel = {
        let label = UILabel()
        let atttributedText = NSMutableAttributedString(string: "11\n", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
        atttributedText.append(NSAttributedString(string: "follows", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)]))
        label.attributedText = atttributedText
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let followingLabel: UILabel = {
        let label = UILabel()
        let atttributedText = NSMutableAttributedString(string: "11\n", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
        atttributedText.append(NSAttributedString(string: "followers", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)]))
        label.attributedText = atttributedText
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    //had to lazy bar episode 26
    lazy var editProfileFollowButton: UIButton = {
        let button = UIButton()
        //button.setTitle("編集", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleEditOrFollow), for: .touchUpInside)
        return button
    }()
    
    lazy var hostRegisterButton: UIButton = {
        let button = UIButton()
        button.setTitle("Become a Host", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleHostRegister), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleHostRegister() {
        print(123)
        delegate?.didTapRegister()
    }
    
    func setupRegisterButton() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        guard let userId = user?.uid else {return}
    
        if uid != userId {
            hostRegisterButton.titleLabel?.isHidden = true
            hostRegisterButton.isHidden = true
        }
        let ref = Database.database().reference().child("users").child(uid)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            //print(snapshot.value)
            
            guard let dictionaries = snapshot.value as? [String: Any] else {return}
            dictionaries.forEach({ (key,value) in
                print(key)
                
                if key == "host"  {
                    self.hostRegisterButton.setTitle("You Are an Host ✔︎", for: .normal)
                    self.hostRegisterButton.backgroundColor = UIColor.mainBlue()
                    self.hostRegisterButton.setTitleColor(.white, for: .normal)
                    self.hostRegisterButton.isEnabled = false
                }
            })
            
        }) { (err) in
            print("failed to fetch data", err)
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(profileImageView)
        profileImageView.anchor(top: topAnchor, left: self.leftAnchor, bottom: nil, right: nil, paddingTop: 12, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, height: 80, width: 80)
        
        profileImageView.layer.cornerRadius = 80 / 2
        profileImageView.layer.masksToBounds = true
        
        setupButtomToolbar()

        addSubview(usernameLabel)
        usernameLabel.anchor(top: profileImageView.bottomAnchor, left: profileImageView.leftAnchor, bottom: gridButton.topAnchor, right: profileImageView.rightAnchor, paddingTop: 4, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        setupUserStatusView()
        addSubview(editProfileFollowButton)
        
        editProfileFollowButton.anchor(top: postLabel.bottomAnchor, left: postLabel.leftAnchor, bottom: nil, right: followingLabel.rightAnchor, paddingTop: 2, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 34, width: 0)
        
        addSubview(hostRegisterButton)
        hostRegisterButton.anchor(top: editProfileFollowButton.bottomAnchor, left: editProfileFollowButton.leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 12, height: 34, width: 0)
    }
    
    fileprivate func setupUserStatusView() {
        let stackView = UIStackView(arrangedSubviews: [postLabel, followersLabel, followingLabel])
        
        //stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        
        addSubview(stackView)
        stackView.anchor(top: topAnchor, left: profileImageView.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 12, paddingLeft: 12, paddingBottom: 0, paddingRight: 12, height: 50, width: 0)
    }
    
    fileprivate func setupButtomToolbar() {
        
        let topDividerView = UIView()
        topDividerView.backgroundColor = UIColor.lightGray
        
        let bottomDividerView = UIView()
        bottomDividerView.backgroundColor = UIColor.lightGray
        
        let stackView = UIStackView(arrangedSubviews: [gridButton, listButton, bookmarkButton, sendmarkButton])
        
        //addSubview(badgeLabel)
        //badgeLabel.anchor(top: bookmarkButton.topAnchor, left: bookmarkButton.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        
        addSubview(stackView)
        addSubview(topDividerView)
        addSubview(bottomDividerView)
        addSubview(badgeLabel)
        
        stackView.anchor(top: nil, left: leftAnchor, bottom: self.bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        topDividerView.anchor(top: stackView.topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 1, width: 0)
        
        bottomDividerView.anchor(top: stackView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        badgeLabel.anchor(top: topDividerView.bottomAnchor, left: nil, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 100, height: 20, width: 20)
        badgeLabel.layer.cornerRadius = 20 / 2
        badgeLabel.layer.masksToBounds = true
        
    }
    //video 18 reduced the codes below due to duplication of code
//    fileprivate func setupProfileImage() {
//
//        guard let profileImageUrl = user?.profileImageUrl else {return}
//        guard let url = URL(string: profileImageUrl) else {return}
//
//        URLSession.shared.dataTask(with: url) { (data, response, err) in
//            if let err = err {
//                print("failed to fetch profile image", err)
//                return
//            }
//
//            //perhaps check the status of 200 (if HTTP is ok)
//
//            guard let data = data else {return}
//            let image = UIImage(data: data)
//
//            DispatchQueue.main.async {
//                self.profileImageView.image = image
//
//            }
//
//            //check the error , then constrcut the image using data
//            }.resume()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
