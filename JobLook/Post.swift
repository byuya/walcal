//
//  Post.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/24.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import Foundation

struct Post {
    //for iserting comments method
    
    var id: String?

    var convert: Double?
    var currencyMark: String?
    
    var count: Int?
    
    let user: User
    let imageUrl: String
    let caption: String
    let title: String
    let category: String
    let place: String
    let language: String
    let participant: Int
    let price: Int
    let profit: Int
    let accountId: String
    let creationDate: Date
    let read: Int
    var hasLiked: Bool = false

    init(user: User, dictionary: [String: Any]) {
        self.user = user
        
        self.imageUrl = dictionary["imageUrl"] as? String ?? ""
        self.caption = dictionary["caption"] as? String ?? ""
        self.title = dictionary["title"] as? String ?? ""
        self.category = dictionary["category"] as? String ?? ""
        self.place = dictionary["place"] as? String ?? ""
        self.language = dictionary["language"] as? String ?? ""
        self.participant = dictionary["participant"] as? Int ?? 0
        self.price = dictionary["price"] as? Int ?? 0
        self.profit = dictionary["profit"] as? Int ?? 0
        self.accountId = dictionary["accountId"] as? String ?? ""
        self.read = dictionary["read"] as? Int ?? 0
        
        //self.scheduleDate = dictionary["scheduleDate"] as? Date
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
    }
}
