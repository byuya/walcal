//
//  CancelAcceptByGuestCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 12/3/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
protocol CancelAcceptByGuestCellDelegate {
    func didTapDismissFromAccept()
    func didTapAccept()
}
class CancelAcceptByGuestCell: UICollectionViewCell {
    
    var delegate: CancelAcceptByGuestCellDelegate?

    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "By clicking 'Accept', a refund will be made to your credit card"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.white
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    lazy var acceptButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Accept", for: .normal)
        //button.titleLabel?.text = "Add Payment"
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleAccept), for: .touchUpInside)
        return button
    }()
    
    @objc func handleAccept() {
        delegate?.didTapAccept()
    }
    
    lazy var dismissButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        //button.titleLabel?.text = "Add Payment"
        button.backgroundColor = UIColor.red
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return button
    }()
    
    @objc func handleDismiss() {
        delegate?.didTapDismissFromAccept()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .orange
        
        setupView()
    }
    
    func setupView() {
        addSubview(titleLabel)
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 5, height: 100, width: 0)
        
        addSubview(acceptButton)
        acceptButton.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        addSubview(dismissButton)
        dismissButton.anchor(top: acceptButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
