//
//  CalenderViewCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/15.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class CalenderViewCell: UICollectionViewCell {
    
    let dateManager = DateManager()
    let daysPerWeek: Int = 7
    let cellMargin: CGFloat = 2.0
    var selectedDate = NSDate()
    var today: NSDate!
    let weekArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    
    
    let textLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HAHAHA", size: 14)
        label.textAlignment = .center
        return label
    }()
    
//    let beforeMonthButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setTitle("＜", for: .normal)
//        return button
//    }()
//
//    let afterMonthButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setTitle("＞", for: .normal)
//        return button
//    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .brown
        
//        addSubview(beforeMonthButton)
//        beforeMonthButton.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
//        addSubview(afterMonthButton)
//        afterMonthButton.anchor(top: topAnchor, left: nil, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(textLabel)
        textLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
