//
//  ExperienceController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/3/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth


//enum selectedText: Int {
//    case title = 0
//    case hostName = 1
//}

class ExperienceController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    let cellId = "cellId"
    let headerId = "headerId"
    
    var postUser: String?
    var postId: String?
    var toId: String?
    
    var initialRateKey: String?
    var initialRateValue: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .yellow
        collectionView.alwaysBounceVertical = true
        
        collectionView.register(ExperienceCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(ExperienceHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        
        //fetchPosts()
        
        fetchUser()
        
        setupSeachBar()
        
    }

    fileprivate func fetchUser() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.currencyConverter(userCurrency: user.currency)
        }
    }
    
    func currencyConverter(userCurrency: String) {
        
        let apiEndPoint = "https://api.exchangeratesapi.io/latest?base=JPY"
        
        guard let url = URL(string: apiEndPoint) else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else {return}
            
            if let httpResponse = response as? HTTPURLResponse {
                guard httpResponse.statusCode == 200 else {return}
                print("Evetying is ok")
            }
            
            guard let data = data else {return}
            
            do {
                
                guard let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {return}
                
                if let rates = dict["rates"] as? [String: Any], let base = dict["base"] as? String, let date = dict["date"] as? String {
                    print(base, date)
                    
                    for (rateKey, rateValue) in rates {
                        if rateKey == userCurrency {
                            
                            //self.initialRateKey = rateKey
                            //self.initialRateValue = rateValue as! Double
                            
                            self.fetchPosts(rateValue: rateValue as! Double, rateKey: rateKey)
                            
                        }
                    }
                }
            } catch {
                print("some error occured")
                return
            }
        }
        task.resume()
    }
    
    var isFinishPaging = false
    var posts = [Post]()
    var filterdPosts = [Post]()
    
    var categoryPosts = [Post]()
    var postUserArray = [String]()
    
    fileprivate func fetchPosts(rateValue: Double, rateKey: String) {

        let ref = Database.database().reference().child("posts")
        //ref.observe(.value, with: { (snapshot) in
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
            guard let dictionaries = snapshot.value as? [String: Any] else {return}
            dictionaries.forEach({ (key,value) in
                
               self.postUserArray.append(key)
                
                //self.readUserComment(postUser: key)
                
        let postRef = Database.database().reference().child("posts").child(key)
                postRef.observe(.childAdded, with: { (snap) in
                    guard let postDictionaries = snap.value as? [String: Any] else {return}
                    //print("want to get postId", snap.key)

                    Database.fetchUserWithUID(uid: key, completion: { (user) in
                        var post = Post(user: user, dictionary: postDictionaries)

                        post.id = snap.key

                        post.convert = rateValue
                        post.currencyMark = rateKey

                        //print("this is conver value", post.convert)
                        self.posts.append(post)
                        
                        self.filterdPosts = self.posts
                        
                        self.collectionView.reloadData()
                    })

                }, withCancel: { (err) in
                    print("failed to fetch posts")
                    return
                })
            })

        }) { (err) in
            print("failed to fetch data", err)
            return
        }
        
        //ref.removeAllObservers()

    }
    
//    fileprivate func fetchPosts(rateValue: Double, rateKey: String) {
//
//        print("initialvalue", initialRateValue ?? 0)
//
//        let ref = Database.database().reference().child("posts")
//        ref.observeSingleEvent(of: .value, with: { (snapshot) in
//
//            guard let dict = snapshot.value as? [String: Any] else {return}
//            dict.forEach({ (key, value) in
//                print("this is key", key)
//
//        let postRef = Database.database().reference().child("posts").child(key)
//
//                var query = postRef.queryOrdered(byChild: "creationDate")
//
//                if self.posts.count > 0 {
//                let value = self.posts.last?.creationDate.timeIntervalSince1970
//                query = query.queryEnding(atValue: value)
//                }
//
//            query.queryLimited(toLast: 4).observe(.value, with: { (snap) in
//
//                guard var allObjects = snap.children.allObjects as? [DataSnapshot] else {return}
//
//                if allObjects.count < 4 {
//                self.isFinishPaging = true
//                }
//
//                if self.posts.count > 0 && allObjects.count > 0 {
//                    allObjects.removeFirst()
//                }
//
//                allObjects.forEach({ (snaps) in
//                    print("let me see this value", snaps.key)
//
//                    guard let dictionary = snaps.value as? [String: Any] else {return}
//                    Database.fetchUserWithUID(uid: key, completion: { (user) in
//                        var post = Post(user: user, dictionary: dictionary)
//                        post.id = snaps.key
//
//                        self.posts.append(post)
//
//                        self.collectionView.reloadData()
//
//                    })
//                })
//
//                }, withCancel: { (err) in
//                    print("failed to fetch last 4 events", err)
//                })
//
//            })
//
//        }) { (err) in
//            print("failed to observe userid", err)
//        }
//
//    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //how to fire off paginate call
//        if indexPath.item == self.posts.count - 1 && !isFinishPaging {
//            print("paginating posts", initialRateValue ?? 0)
//            self.fetchPosts(rateValue: initialRateValue ?? 0, rateKey: initialRateKey ?? "")
//        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ExperienceCell
        cell.post = filterdPosts[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterdPosts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        searchBar.isHidden = true
        searchBar.resignFirstResponder()
        
        let post = filterdPosts[indexPath.item]
        print("this is postUser", post.user.uid)
        
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        postController.post = post
        navigationController?.pushViewController(postController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (view.frame.width - 2) / 3, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath)
            as! ExperienceHeader
        //header.delegate = self
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 70)
        
    }
    
    func setupSeachBar() {
        
//        let height: CGFloat = 50
//        let bounds = self.navigationController?.navigationController?.navigationBar.bounds
//        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds?.width ?? 0, height: bounds?.height ?? 0 + height)
        
        //navigationController?.navigationBar.addSubview(searchBar)
        let navBar = navigationController?.navigationBar
        navBar?.addSubview(searchBar)
        searchBar.anchor(top: navBar?.topAnchor, left: navBar?.leftAnchor, bottom: navBar?.bottomAnchor, right: navBar?.rightAnchor, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, height: 0, width: 0)
        
    }
    
    lazy var searchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Search"
        sb.tintColor = .gray
        //sb.showsScopeBar = true
        //sb.scopeButtonTitles = ["Tour", "Host"]
        //sb.selectedScopeButtonIndex = 0
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230)
        sb.delegate = self
        return sb
    }()
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            filterdPosts = posts
            //self.collectionView.reloadData()
        }else {
            filterdPosts = self.posts.filter { (post) -> Bool in
                return post.title.lowercased().contains(searchText.lowercased()) || post.place.lowercased().contains(searchText.lowercased()) || post.user.username.lowercased().contains(searchText.lowercased())
                //filterPostsInNameAndHost(index: searchBar.selectedScopeButtonIndex, text: searchText)
            }
        }
        
        self.collectionView?.reloadData()
    }
    
//    func filterPostsInNameAndHost(index: Int, text: String) {
//        switch index {
//        case selectedText.title.rawValue:
//            filterdPosts = self.posts.filter({ (post) -> Bool in
//                return post.title.lowercased().contains(text.lowercased())
//            })
//            self.collectionView.reloadData()
//
//        case selectedText.hostName.rawValue:
//            filterdPosts = self.posts.filter({ (post) -> Bool in
//                print("postUser", post.user.uid)
//                return post.user.uid.lowercased().contains(text.lowercased())
//            })
//
//            self.collectionView.reloadData()
//
//        default:
//            print("no type")
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        searchBar.isHidden = false
        searchBar.resignFirstResponder()
        collectionView.keyboardDismissMode = .onDrag
      
    }
    
}
