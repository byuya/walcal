//
//  CreateMessageCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/09.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class CreateMessageCell: UICollectionViewCell {
    
    var message: Message? {
        didSet {
            
            guard let message = message else {return}
            usernameLabel.text = message.user?.username
            //usernameLabel.text = user?.username
            
            guard let profileImageUrl = message.user?.profileImageUrl else {return}
            
            //guard let profileImageUrl  = user?.profileImageUrl else {return}
            profileImageView.loadImage(urlString: profileImageUrl)
    }
}
    let profileImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.backgroundColor = .green
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "UserName here"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        addSubview(profileImageView)
        profileImageView.anchor(top: nil, left: leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 50, width: 50)
        profileImageView.layer.cornerRadius = 50 / 2
        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(usernameLabel)
        usernameLabel.anchor(top: topAnchor, left: profileImageView.rightAnchor, bottom: nil, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView)
        seperatorView.anchor(top: nil, left: usernameLabel.leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
