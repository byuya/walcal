//
//  Schedule.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/11.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FSCalendar

class ScheduleViewController: UICollectionViewController {
    
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
        //button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
        button.setTitle("V", for: .normal)
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        return button
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.text = "2018/10/18"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.lightGray
        return label
    }()
    
    let fromTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "19:00"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.lightGray
        return label
    }()
    
    let toTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "20:00"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = .white
        return label
    }()
    
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tokyo Walk"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.lightGray
        return label
    }()
    
    
    let saveButton: UIButton = {
        let button = UIButton(type: .system)
        //button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
        button.setTitle("Save", for: .normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 80 / 2
        button.backgroundColor =  UIColor.blue
        button.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleSave() {
        print("Handling Save")
        
        guard let date = dateLabel.text else {return}
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let fromTime = fromTimeLabel.text else {return}
        guard let toTime = toTimeLabel.text else {return}
        guard let title = titleLabel.text else {return}
        
        let value = ["fromTime": fromTime, "toTime": toTime, "title": title]
        
        let ref = Database.database().reference().child("schedule").child(uid).child(date)
        ref.childByAutoId().updateChildValues(value) { (err, ref) in
            if let err = err {
                print("failed to insert date", err)
            }
            //if successful
            print("Successfully insered into db")
        }
        
    }
    
        lazy var fromTimePicker: UIDatePicker = {
            let picker = UIDatePicker()
            //picker.datePickerMode = .date
            picker.datePickerMode = .time
            picker.minuteInterval = 30
            //picker.minuteInterval = 10
            //timeView.inputView = picker
            picker.addTarget(self, action: #selector(handleFromSelectedTime), for: .valueChanged)
            return picker
        }()
    
        @objc func handleFromSelectedTime() -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let selectTime = dateFormatter.string(from: fromTimePicker.date)
            //dateAndTimeLabel.inputView = timePicker
            fromTimeLabel.text = selectTime
            print(selectTime)
            //toolBarSetup()
            return selectTime
        }
    
    lazy var toTimePicker: UIDatePicker = {
        let picker = UIDatePicker()
        //picker.datePickerMode = .date
        picker.datePickerMode = .time
        picker.minuteInterval = 30
        //picker.minuteInterval = 10
        //timeView.inputView = picker
        picker.addTarget(self, action: #selector(handlToSelectedTime), for: .valueChanged)
        return picker
    }()
    
    @objc func handlToSelectedTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let selectTime = dateFormatter.string(from: toTimePicker.date)
        //dateAndTimeLabel.inputView = timePicker
        toTimeLabel.text = selectTime
        print(selectTime)
        //toolBarSetup()
        return selectTime
    }
    
    @objc fileprivate func handleBack() {
        print("handling add buton")
        
        dismiss(animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .yellow
        
        setupView()
        
    }
    
  
    func setupView() {
        collectionView.addSubview(backButton)
        backButton.anchor(top: collectionView.topAnchor, left: collectionView.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        collectionView.addSubview(dateLabel)
        dateLabel.anchor(top: backButton.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        collectionView.addSubview(fromTimePicker)
        fromTimePicker.anchor(top: dateLabel.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 100, width: 0)
        
        collectionView.addSubview(fromTimeLabel)
        fromTimeLabel.anchor(top: fromTimePicker.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        collectionView.addSubview(toTimePicker)
        toTimePicker.anchor(top: fromTimeLabel.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 100, width: 0)
        
        collectionView.addSubview(toTimeLabel)
        toTimeLabel.anchor(top: toTimePicker.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        collectionView.addSubview(titleLabel)
        titleLabel.anchor(top: toTimeLabel.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 50, width: 0 )
        
        collectionView.addSubview(saveButton)
        saveButton.anchor(top: titleLabel.bottomAnchor, left: nil, bottom: nil, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 80, width: 80)
    }
}
