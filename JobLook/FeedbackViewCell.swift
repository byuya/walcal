//
//  FeedbackViewCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/23/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

protocol FeedbackViewCellDelegate {
    func didTapCancel(for cell: FeedbackViewCell)
    
}

class FeedbackViewCell: UICollectionViewCell {
    
    var delegate: FeedbackViewCellDelegate?
    
    var post: Post? {
        didSet {
            titleLabel.text = post?.title

        }
    }
    
    var experiment: Experiment? {
        didSet {
            
            feedbackLabel.text = "Please provide feedback!"
            titleLabel.text = experiment?.title

        }
    }
    let feedbackLabel: UILabel = {
        let label = UILabel()
        label.text = "Please provide feedback!"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tour Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
//    lazy var cancelButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setTitle("Cancel", for: .normal)
//        button.backgroundColor = UIColor.magenta
//        button.setTitleColor(UIColor.lightGray, for: .normal)
//        button.titleLabel?.textAlignment = .left
//        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
//        button.layer.cornerRadius = 5
//        button.addTarget(self, action: #selector(handleGiveFeedBack), for: .touchUpInside)
//        return
//    }()
    
    lazy var cancelRequestButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel Request", for: .normal)
        button.backgroundColor = UIColor.magenta
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleRequestCancel), for: .touchUpInside)
        return button
    }()
    
    @objc  fileprivate func handleRequestCancel() {
        delegate?.didTapCancel(for: self)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(feedbackLabel)
        feedbackLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 40, width: 0)
        
//        addSubview(cancelButton)
//        cancelButton.anchor(top: feedbackLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: <#T##CGFloat#>, width: <#T##CGFloat#>)
        
        addSubview(titleLabel)
        titleLabel.anchor(top: feedbackLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(cancelRequestButton)
        cancelRequestButton.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 50, width: 0)
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
