//
//  FirebaseUtils.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/25.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseDatabase

extension Database {
    static func fetchUserWithUID(uid: String, completion: @escaping (User) -> ()){
        //print("fetchin user with uid", uid)
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let userDictionary = snapshot.value as? [String: Any] else { return }
            
            let user = User(uid: uid, dictionary: userDictionary)
            
            //print(user.username)
            
            completion(user)
            
            //let user = User(dictionary: userDictionary)
            //self.fetchPostWithUser(user: user)
            
        }) { (err) in
            print("failed to fetch users", err)
        }
    }
    
    static func fetchPostWithId(postUser: String, postId: String, completion: @escaping (TestPost) -> ()) {
        
        Database.database().reference().child("posts").child(postUser).child(postId).observeSingleEvent(of: .value, with: { (snap) in
            
            guard let dictionary = snap.value as? [String: Any] else {return}
            
                    let testPost = TestPost(postUser: postUser, postId: postId, dictionary: dictionary)
                    
                    completion(testPost)
            
        }) { (err) in
            print("failed to fetch post", err.localizedDescription)
            return
        }

    }
}
