//
//  Experiment.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/24/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import Foundation

struct Experiment {
    
    var id: String?
    var hostId: String?
    var guestId: String?
    var amount: Int?
    
    let user: User
    let price: Int
    //let amount: Int
    let accountId: String
    let title: String
    var chargeId: String
    let caption: String
    let place: String
    let guestName: String
    let cancelRequest: Int
    let feedBack: Int
    let balanceTransaction: String
    let status: String
    let created: Date
    let creationDate: Date
    let expireDate: Date
    let read: String
    
    init(user: User, dictionary: [String: Any]) {
        self.user = user
        
        //inside metadata
        self.accountId = dictionary["accountId"] as? String ?? ""
        self.cancelRequest = dictionary["cancelRequest"] as? Int ?? 0
        self.caption = dictionary["caption"] as? String ?? ""
        self.place = dictionary["place"] as? String ?? ""
        self.read = dictionary["read"] as? String ?? ""
        self.title = dictionary["title"] as? String ?? ""
        // inside metadata
        
        self.price = dictionary["price"] as? Int ?? 0
        self.amount = dictionary["amount"] as? Int ?? 0
        self.chargeId = dictionary["id"] as? String ?? ""
        self.guestName = dictionary["guestName"] as? String ?? ""
        self.balanceTransaction = dictionary["balance_transaction"] as? String ?? ""
        self.status = dictionary["status"] as? String ?? ""
        self.feedBack = dictionary["feedBack"] as? Int ?? 0
        
        let created = dictionary["created"] as? Double ?? 0
        self.created = Date(timeIntervalSince1970: created)
        
        let secondsFrom1970Create = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970Create)
        
        let secondsFrom1970Expire = dictionary["expireDate"] as? Double ?? 0
        self.expireDate = Date(timeIntervalSince1970: secondsFrom1970Expire)
    
        
    }
}
