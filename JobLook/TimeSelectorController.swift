//
//  TimeSelectorController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/16.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

protocol TimeSelectorControllerDelegate {
    
    func updateFromTime()

}

class TimeSelectorController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    
    let cellId = "cellId"
    let headerId = "headerId"
    
    //let timeSelectorController = TimeSelectorController()
    
    let timeSelectorCell = TimeSelectorCell()
    
    var timeDelegate: TimeSelectorControllerDelegate?
    var selectedName: String = "Anonymus"
    //var delegate: SharePhotoController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = .yellow
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleTimeSave))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "V", style: .plain, target: self, action: #selector(handleBack))

        collectionView?.register(TimeSelectorCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView.register(TimeSelectorHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
    
    }
    
    
    @objc private func handleTimeSave() {
        print(123)
        timeDelegate?.updateFromTime()
        
        //let time = Time(timeText: cell.fromTextView.text, selectTime: Date())
    }
    
    @objc fileprivate func handleBack() {
        dismiss(animated: true, completion: nil)
    }
    
    var array: [String] = ["First", "Second", "Third"]
    var date = [Date]()
    
    //var times = [Time]()
    var time: Time?
    
//    func updateTime(indexPath: IndexPath) -> String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "h:mm a"
//
//        self.collectionView.reloadItems(at: [indexPath])
//        
//        return dateFormatter.string(from: date[indexPath.row] as Date)
//
//    }
    
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! TimeSelectorHeader
        if indexPath.section == 0 {
            header.headerLabel.text = "First"
        } else if indexPath.section == 1 {
            header.headerLabel.text = "Second"
        } else if indexPath.section == 2 {
            header.headerLabel.text = "Third"
        }
    
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    
    }
    

    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TimeSelectorCell
        
//        enum TimeSetting: String {
//            case cellFirst
//            case cellSecond
//            case cellThird
//
//            func timeTextFunction(cell: TimeSelectorCell) -> String{
//                let timeSelectorCell = TimeSelectorCell()
//                let timeText = timeSelectorCell.handleFromSelectedTime()
//                cell.fromTextView.text = timeText
//                print(timeText)
//                return timeText
//            }
//        }
//
//        enum Weekday: String {
//            case Monday
//            case Tuesday
//            case Thursday
//
//            func day() -> String {
//                return self.rawValue
//            }
//
//        }
//
//        print(Weekday.Monday.day())
        
//        switch cell.fromTextView.tag {
//        case 0:
//            cell.fromTextView.text = "this is case 0"
//        default:
//            cell.fromTextView.text = "this is default case"
//        }
        if indexPath.section == 0 {
            cell.fromTextView.inputView = timeSelectorCell.fromTimePicker
            cell.fromTextView.text = timeSelectorCell.handleFromSelectedTime()
            
            cell.fromTextView.backgroundColor = .blue
            //cell.delegate = self
            return cell
        }
        
            updateDate(cell: cell)
    
            updateToDate(cell: cell)
        
        
        //cell.delegate = self
  
        return cell
    }

    func updateDate(cell: TimeSelectorCell) {
        
            cell.fromTextView.backgroundColor = UIColor.orange
            cell.fromTextView.inputView = timeSelectorCell.fromTimePicker
            cell.fromTextView.text = timeSelectorCell.handleFromSelectedTime()
            timeSelectorCell.fromTimePicker.addTarget(self, action: #selector(handleFrom), for: .valueChanged)
        
        guard let text = cell.fromTextView.text else {return}
        print(text)
        //delegate?.updateFromTime(for: text)
            
    }
    
    @objc fileprivate func handleFrom(cell: TimeSelectorCell) {
            
        self.collectionView.reloadData()
        
        //delegate?.updateFromTime(for: text!)
    }
    
    
    func updateToDate(cell: TimeSelectorCell) {
        cell.toTextView.backgroundColor = .cyan
        cell.toTextView.inputView = timeSelectorCell.toTimePicker
        cell.toTextView.text = timeSelectorCell.handleToSelectedTime()
        timeSelectorCell.toTimePicker.addTarget(self, action: #selector(handleTo), for: .valueChanged)
    }
    
    @objc fileprivate func handleTo(cell: TimeSelectorCell) {
        
        self.collectionView.reloadData()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            return CGSize(width: view.frame.width, height: 100)
        
    }
    
//    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TimeSelectorCell
//
//        cell.fromTextView.text = "this is didselect text"
//        if indexPath.row == 0  {
//            cell.fromTextView.backgroundColor = UIColor.orange
//            cell.fromTextView.inputView = timeSelectorCell.fromTimePicker
//            cell.fromTextView.text = timeSelectorCell.handleFromSelectedTime()
//            timeSelectorCell.fromTimePicker.addTarget(self, action: #selector(handleFrom), for: .valueChanged)
//
//        }
//
//
//    }
//
//    fileprivate func setupIndexPath0(cell: TimeSelectorCell) {
//        //cell.delegate = self
//    }
    
}

    

