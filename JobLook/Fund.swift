//
//  Fund.swift
//  JobLook
//
//  Created by Akiya Ozawa on 12/11/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import Foundation

struct Fund {
    
    var totalofAmount: Int?
    
    var id: String?
    
    let user: User
    let amount: Int
    let guestName: String
    let title: String
    let transferDate: Date
    let payoutProcess: Int
    init(user: User, dictionary: [String: Any]) {
        self.user = user
        
       self.amount = dictionary["amount"] as? Int ?? 0
        self.guestName = dictionary["guestName"] as? String ?? ""
        self.title = dictionary["title"] as? String ?? ""
        self.payoutProcess = dictionary["payout_process"] as? Int ?? 0
        let secondsFrom1970Create = dictionary["creationDate"] as? Double ?? 0
        self.transferDate = Date(timeIntervalSince1970: secondsFrom1970Create)
    }
}
