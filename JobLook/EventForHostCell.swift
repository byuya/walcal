//
//  EventForHostCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/6/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

class EventForHostCell: UICollectionViewCell {
    
    var experiment: Experiment? {
        didSet {
            
            titleLabel.text = experiment?.title
    
            if experiment?.read == "1" {
                badgeLabel.backgroundColor = .red
                badgeLabel.text = "New"
            } else {
                 badgeLabel.backgroundColor = .white
            }
            
        }
    }
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tour Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    let purchaseLabel: UILabel = {
        let label = UILabel()
        label.text = "Purchaser"
        label.font = UIFont.boldSystemFont(ofSize: 10)
        label.textColor = UIColor.lightGray
        return label
    }()
    
    let badgeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 0.5)
        label.backgroundColor = .red
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "New"
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        setupView()
    }
    
    func setupView() {
        addSubview(titleLabel)
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(purchaseLabel)
        purchaseLabel.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(badgeLabel)
        badgeLabel.anchor(top: topAnchor, left: nil, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 20, height: 30, width: 50)
        badgeLabel.layer.cornerRadius = 30 / 2
        badgeLabel.layer.masksToBounds = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
