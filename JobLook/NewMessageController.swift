//
//  NewMessengeController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/08.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

//import UIKit
//import Firebase
//
//class NewMessageController: UITableViewController {
//
//    let cellId = "cellId"
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "＜", style: .plain, target: self, action: #selector(handleCancel))
//
//        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
//
//        fetchUsers()
//    }
//
//    var userForMessage = [UserForMessage]()
//
//     func fetchUsers() {
//
//        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
//            if let dictionary = snapshot.value as? [String: AnyObject] {
//                let user = UserForMessage()
//                user.setValuesForKeys(dictionary)
//                print(user.username)
//            }
//        }, withCancel: nil)
////        let ref = Database.database().reference().child("users")
////        ref.observeSingleEvent(of: .value, with: { (snapshot) in
////            print(snapshot)
////
////            guard let dictionary = snapshot.value as? [String: Any] else {return}
////            dictionary.forEach({ (key, value) in
////
////                guard let userDictionary = value as? [String: Any] else {return}
////                let user = UserForMessage()
////                self.userForMessage.append(user)
////
////                DispatchQueue.main.async {
////                self.tableView.reloadData()
////                }
////            })
////        }) { (err) in
////            print("failed to get user info")
////        }
//    }
//
//    @objc func handleCancel() {
//        navigationController?.dismiss(animated: true, completion: nil)
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return userForMessage.count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        //let cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
//        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) //as! UserCell
//        let user = userForMessage[indexPath.row]
//        cell.textLabel?.text = user.username
//
//        cell.imageView?.image = UIImage(named: "profile_selected")
//
//        if let profileImageUrl = user.profileImageUrl {
//            let url = URL(fileURLWithPath: profileImageUrl)
//            URLSession.shared.dataTask(with: url) { (data, response, err) in
//                if err != nil{
//                    print(err)
//                    return
//                }
//                DispatchQueue.main.async {
//                cell.imageView?.image = UIImage(data: data!)
//            }
//
//        }.resume()
//
//    }
//        return cell
//
//}
//
//class UserCell: UITableViewCell {
//
//    let profileImageView: UIImageView = {
//        let imageView = UIImageView()
//        imageView.contentMode = .scaleAspectFill
//        //imageView.backgroundColor = .red
//        imageView.clipsToBounds = true
//        return imageView
//    }()
//
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
//
//        addSubview(profileImageView)
//        profileImageView.anchor(top: nil, left: leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, height: 40, width: 40)
//        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}
//
//}
