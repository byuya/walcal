//
//  ExperienceHeader.swift
//  JobLook
//
//  Created by Akiya Ozawa on 11/4/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

class ExperienceHeader: UICollectionViewCell {
    
    let titleLabel: UILabel = {
        let cl = UILabel()
        cl.text = "Find Experience to Explore!"
        cl.font = UIFont.boldSystemFont(ofSize: 25)
        cl.textColor = UIColor.gray
        cl.textAlignment = .left
        return cl
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        setupView()
    }
    
    func setupView() {
        addSubview(titleLabel)
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
