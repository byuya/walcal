//
//  FSCalenderCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/10/17.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class FSCalenderCell: UICollectionViewCell {
    
    let fsCalenderheader = FSCalendarHeader()
    
    var schedule: Schedule? {
        didSet {
            
            scheduleLabel.text = schedule?.time
            //scheduleLabel.text = fsCalenderheader.scheduleHeaderLabel
            
        }
    }
    let scheduleLabel: UILabel = {
        let label = UILabel()
        label.text = "8:00~9:00 I will do this"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .left
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //backgroundColor = .yellow
        
        addSubview(scheduleLabel)
        scheduleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
