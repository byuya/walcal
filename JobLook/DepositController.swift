//
//  DepositController.swift
//  JobLook
//
//  Created by Akiya Ozawa on 12/11/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import Stripe

class DepositController: UICollectionViewController, UICollectionViewDelegateFlowLayout, DepositCellDelegate {
    
    let cellId = "cellId"
    let headerId = "headerId"
    
    var fund: Fund?
    var total: Int?
    var bankId: String?
    var fundId: String?
    
    var fundArraIds = [String?]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .red
        
        collectionView.register(DepositCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(DepositHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        
        print("this is total number of host has", total ?? 0)
        
        fetchBankInfo()
        
        //check()
    }
    
    func check() {
        
         guard let uid = Auth.auth().currentUser?.uid else {return}
        
        for id in fundArraIds {
            //print("these are ids inside fundArray", id)
            guard let id = id else {return}
            let ref = Database.database().reference().child("users").child(uid).child("funds").child(id)
            ref.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
                if var data = currentData.value as? [String: Any] {
                    var count = data["payout_process"] as! Int
                    count = 1
                    data["payout_process"] = count
                    currentData.value = data
                    print("let me this what value prints out", currentData)
                    return TransactionResult.success(withValue: currentData)
                }
                
                return TransactionResult.success(withValue: currentData)
            }
        }
    }
    
    var banks = [Bank]()
    
    func fetchBankInfo() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let bankRef = Database.database().reference().child("users").child(uid).child("bank")
        bankRef.observe(.childAdded, with: { (snapshot) in
            
            //print("i want to get bank id start ba_", snapshot.value)
            guard let dictionary = snapshot.value as? [String: Any] else {return}
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                let bank = Bank(user: user, dictionary: dictionary)
                self.bankId = bank.bankId
                
                self.banks.append(bank)
                
                self.collectionView.reloadData()
                
            })
        }) { (err) in
            
            print("failed to fetch bank info", err)
        }
    }
    
    func didTapDeposit() {
        
        print("Processing deposit request", bankId ?? "", "this is total amout that will be paid out", total ?? 0)
        
        guard let amount = total else {return}
        guard let destination = bankId else {return}
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let value = ["amount": amount, "destination": destination] as [String: Any]
        
        let payoutRef = Database.database().reference().child("users").child(uid).child("payout")
        payoutRef.childByAutoId().updateChildValues(value) { (err, ref) in
            if let err = err {
                print("failed to insert payout information", err)
                return
            }
            
            print("Successfully inserted patout info")
            self.check()
            //self.deleteFundsData()
        }
        
    }
    
//    func deleteFundsData() {
//
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//
//        let deleteRef = Database.database().reference().child("users").child(uid).child("funds")
//        deleteRef.removeValue { (err, ref) in
//            if let err = err {
//                print("failed to remove", err)
//                return
//            }
//            
//            print("Successfully removed the data")
//        }
//    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! DepositHeader
        guard let total = total else {return header}
        header.priceLabel.text = String(total)
        header.delegate = self
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 200)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DepositCell
        cell.bank = banks[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return banks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 60)
    }
    
}
