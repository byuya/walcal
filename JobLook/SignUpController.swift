//
//  ViewController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/07/13.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Stripe
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseMessaging
import FirebaseStorage
import GoogleSignIn

class SignUpController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
    
    //登録ボタン
    let plusPhotoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handlePlusPhoto), for: .touchUpInside)
        return button
    }()
    
    @objc func handlePlusPhoto() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            plusPhotoButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
            
            //info["UIImagePickerControllerOriginalImage"] we had it before Swift 4.2
        } else if let originalImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            
            plusPhotoButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        plusPhotoButton.layer.cornerRadius = plusPhotoButton.frame.width/2
        plusPhotoButton.layer.masksToBounds = true
        plusPhotoButton.layer.borderColor = UIColor.black.cgColor
        plusPhotoButton.layer.borderWidth = 3
        dismiss(animated: true, completion: nil)
    }
    
    let googleSignInButton: GIDSignInButton = {
        let button = GIDSignInButton()
        return button
        
    }()
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let err = error {
            print("failed to logged into Google", err)
            return
        }
        
        print("Successfully logged into Google", user)
        
        
        guard let idToken = user.authentication.idToken else {return}
        guard let accessToken = user.authentication.accessToken else {return}
        
        let credentials = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
        Auth.auth().signInAndRetrieveData(with: credentials) { (user, error) in
            
            if let err = error {
                print("Failed to create a Firebase User with with Google Account", err)
            }
            
            guard let uid = user?.user.uid else {return}
            print("Successfully logged in with Google account", uid)
            
            guard let mainTabController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else {return}
            mainTabController.setupViewControllers()
            self.dismiss(animated: true, completion: nil)
            
//            let paymentController = PaymentController(collectionViewLayout: UICollectionViewFlowLayout())
//            self.present(paymentController, animated: true, completion: nil)
            
        }
        
    }
    
    
    //メールテキスト
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "メールアドレス"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    @objc func handleTextInputChange() {
        
        let isFormValid = emailTextField.text?.count ?? 0 > 0 && usernameTextField.text?.count ?? 0 > 0 && passwordTextField.text?.count ?? 0 > 0
        
        if isFormValid {
            signupButton.isEnabled = true
            signupButton.backgroundColor = .mainBlue()

        } else {
            signupButton.isEnabled = false
            signupButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        }
    }
    
    let usernameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "名前"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "パスワード"
        tf.isSecureTextEntry = true
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    let signupButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("登録", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleSignup), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    
    @objc func handleSignup() {
        
        guard let email = emailTextField.text, email.count > 0 else {return}
        guard let username = usernameTextField.text, username.count > 0 else {return}
        guard let password = passwordTextField.text, password.count > 0 else {return}
        
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error: Error?) in
            if let err = error {
                print("failed to create user", err)
                return
            }
            
             print("successfully create user", user?.user.uid ?? "")
            
//      //Eメール承認ファンクション via Firebase
//            Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
//                if let err = error {
//            //some codes that delete users who don't verify
//                    print("failed to send verification email", err)
//                    return
//                }
//            })

        
           guard let image = self.plusPhotoButton.imageView?.image else {return}
           
            guard let uploadData = image.jpegData(compressionQuality: 0.3) else {return}
           
                let fileName = NSUUID().uuidString
                let storageRef = Storage.storage().reference().child("profile_images").child(fileName)
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, err) in
                
                if let err = err {
                    print("failed to upload profile image:", err)
                    return
                }
                
                storageRef.downloadURL(completion: { (downloadURL, err) in
                    guard let profileImageURL = downloadURL?.absoluteString else {return}
                    //                    Storage.storage().reference().child("profile_images").child(fileName).downloadURL(completion: { (url, err) in
                    //                        if let err = err {
                    //                            print("Failed to get downloadurl", err)
                    //                            return
                    //                        }
                    //guard let profileImageURL = url?.absoluteString else {return}
                    print("Successfully uploaded profile image", profileImageURL)
                    
                    //guard let profileImageURL = metadata?.downloadURL()?.absoluteString else {return}
                    
                    //guard let uid = user?.uid else {return}
                    guard let uid = user?.user.uid else {return}
                    guard let fcmToken = Messaging.messaging().fcmToken else {return}
                    
                    let disctionaryValues = ["username": username, "profileImagerl": profileImageURL, "fcmToken": fcmToken]
                    let values = [uid: disctionaryValues]
                    
                    Database.database().reference().child("users").updateChildValues(values, withCompletionBlock: { (err, ref) in
                        if let err = err {
                            print("failed to save user info into db", err)
                            return }
                        
                        print("Successfully save user info into db")
                        
                        guard let mainTabController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else {return}
                        mainTabController.setupViewControllers()
                        self.dismiss(animated: true, completion: nil)
                        
                    })
                })
        
            })
        })
        
}
    
    let alreadyHaveAccountButton: UIButton = {
        let button = UIButton(type: .system)
        
        let attributedText =  NSMutableAttributedString(string: "既にアカウントをお持ちですか？ ", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        attributedText.append(NSAttributedString(string: "ログインする", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.rgb(red: 17, green: 154, blue: 237)]))
        
        
        button.setAttributedTitle(attributedText, for: .normal)
         button.addTarget(self, action: #selector(handleAlradyHaveAccount), for: .touchUpInside)
        return button
    }()
    
    @objc func handleAlradyHaveAccount() {
        navigationController?.popViewController(animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.emailTextField.resignFirstResponder()
        self.usernameTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance()?.delegate = self
        
        view.addSubview(alreadyHaveAccountButton)
        alreadyHaveAccountButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 50, width: 0)
        
        view.backgroundColor = .white
        
        view.addSubview(plusPhotoButton)
        plusPhotoButton.anchor(top: view.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 140, width: 140)
            plusPhotoButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        view.addSubview(googleSignInButton)
        googleSignInButton.anchor(top: plusPhotoButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 60, paddingBottom: 0, paddingRight: 60, height: 0, width: 0)
    
        setupInputFields()

    }
    
    
    fileprivate func setupInputFields(){
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField, usernameTextField, passwordTextField, signupButton])
    
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 10

        view.addSubview(stackView)
    
        stackView.anchor(top: googleSignInButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 40, paddingBottom: 0, paddingRight: 40, height: 200, width: 0)
    }
    
}

