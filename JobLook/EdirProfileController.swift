//
//  EdirProfileController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/12.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit

class EditProfileController: UICollectionViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .brown
    }
}
