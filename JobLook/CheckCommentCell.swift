//
//  CheckCommentCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 1/29/31 H.
//  Copyright © 31 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

class CheckCommentCell: UICollectionViewCell {
    
    var post: Post? {
        didSet {
            
            eventNameLabel.text = post?.title
            
            guard let badgeCount = post?.count else {return}
            let badgeString = String(badgeCount)
            badgeLabel.text = badgeString
            
        }
    }
    
    var commentHistory: CommentHistory? {
        didSet {
            
            //eventNameLabel.text = commentHistory?.title
            
            eventNameLabel.text = commentHistory?.testPost.title
            guard let sender = commentHistory?.user.username else {return}
            senderName.text = "From: \(sender)"
            
            guard let badgeCount = commentHistory?.read else {return}
            
            if badgeCount == 0 {
                badgeLabel.backgroundColor = .white
            } else {
                badgeLabel.backgroundColor = .green
                let badgeString =  String(badgeCount)
                badgeLabel.text = badgeString
            }
           
        }
    }
    
    let eventNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Tokyo Tour"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let senderName: UILabel = {
        let label = UILabel()
        label.text = "SenderName"
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = UIColor.lightGray
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let badgeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(white: 0, alpha: 0.5)
        label.backgroundColor = .red
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = "2"
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(eventNameLabel)
        eventNameLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(senderName)
        senderName.anchor(top: eventNameLabel.bottomAnchor, left: eventNameLabel.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(badgeLabel)
        badgeLabel.anchor(top: topAnchor, left: nil, bottom: nil, right: rightAnchor, paddingTop: 15, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, height: 25, width: 25)
        badgeLabel.layer.cornerRadius = 25 / 2
        badgeLabel.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

