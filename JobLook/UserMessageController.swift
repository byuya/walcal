//
//  UserMessageController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/08.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

//import UIKit
//import Firebase
//
//class UserMessageController: UICollectionViewController {
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        collectionView.backgroundColor = .white
//        navigationItem.title = "受信トレイ"
//        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "メッセ", style: .plain, target: self, action: #selector(handleNewMessage))
//        
//        fetchUserName()
//    }
//    
//    @objc func handleNewMessage() {
//        let newMessageController = NewMessageController()
//        let navController = UINavigationController(rootViewController: newMessageController)
//        present(navController, animated: true, completion: nil)
//        
//    }
//    
//    func fetchUserName() {
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
//            if let dictionary = snapshot.value as? [String: Any] {
//                self.navigationItem.title = dictionary["username"] as? String
//            }
//        }) { (err) in
//            print("faeld to get username", err)
//        }
//    }
//}
