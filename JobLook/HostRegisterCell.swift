//
//  HostRegisterCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/22/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

protocol HostRegisterCellDelegate {
    func didTapAcceptanceTerm()
    
    func didTapSubmit(
        for firstNameText: String,
        for lastNameText: String,
        for genderText: String,
        for phoneText: String,
        for dateBirthYearText: String,
        for dateBirthMonthText: String,
        for dateBirthDayText: String,
        for addressKanjiStateText: String,
        for addressKanjiCityText: String,
        for addressKanjiTownText: String,
        for addressKanaStateText: String,
        for addressKanaCityText: String,
        for addressKanaTownText: String,
        for numberText: String,
        for postalText: String)
    
    func didTapBankSubmit(for accountNumberText: String, for routingNumberText: String, for accountNameText: String)
}
class HostRegisterCell: UICollectionViewCell {
    
    var delegate: HostRegisterCellDelegate?
    //var pickerDelegate: UIPickerViewDelegate?
    //var pickerDatasource: UIPickerViewDataSource?
    
     let gender = ["Male", "Female"]
    
    let pickerView: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()
    
    let becomeHostLabel: UILabel = {
        let label = UILabel()
        label.text = "Please provide your information"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        //label.backgroundColor = UIColor.gray
        label.numberOfLines = 0
        return label
    }()
    
    lazy var firstNameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "FirstName"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var lastNameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "LastName"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var genderTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Gender"
        //tf.isEnabled = false
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    let genderTextView: UITextView = {
        let cv = UITextView()
        cv.text = "Gender"
        cv.backgroundColor = UIColor(white: 0, alpha: 0.03)
        cv.font = UIFont.systemFont(ofSize: 14)
        //cv.textAlignment = .right
        cv.textColor = UIColor.lightGray
        cv.isEditable = false
        cv.layer.cornerRadius = 5
        return cv
    }()
    
    lazy var phoneNumberText: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Phone Number"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    lazy var dateBirthYear: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Date of Birth (Ex:1992)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    lazy var dateBirthMonth: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Month"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    lazy var dateBirthDay: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Day"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
//    let addressCountry: UITextField = {
//        let tf = UITextField()
//        tf.placeholder = "Country"
//        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
//        tf.font = UIFont.systemFont(ofSize: 14)
//        tf.borderStyle = .roundedRect
//        return tf
//    }()
    
    lazy var addressStateKanjiTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "State(kanji)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var addressStateKanaTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "State(kana)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var addressCityKanjiTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "City(Kanji)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var addressCityKanaTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "City(kana)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var addressTownKanjiTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Town(kanji)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var addressTownKanaTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Town(kana)"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var addressNumber: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Street Number"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    lazy var postalNumber: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Postal-Code"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    @objc fileprivate func handleTextInputChange() {
        
        let formIsValid = lastNameTextField.text?.count ?? 0 > 0 && firstNameTextField.text?.count ?? 0 > 0 && genderTextField.text?.count ?? 0 > 0 && phoneNumberText.text?.count ?? 0 > 0 && dateBirthYear.text?.count ?? 0 > 0 && dateBirthMonth.text?.count ?? 0 > 0 && dateBirthDay.text?.count ?? 0 > 0 && addressStateKanjiTextField.text?.count ?? 0 > 0 && addressStateKanaTextField.text?.count ?? 0 > 0 && addressCityKanjiTextField.text?.count ?? 0 > 0 && addressCityKanaTextField.text?.count ?? 0 > 0 && addressTownKanjiTextField.text?.count ?? 0 > 0 && addressTownKanaTextField.text?.count ?? 0 > 0 && addressNumber.text?.count ?? 0 > 0 && postalNumber.text?.count ?? 0 > 0
    
        if formIsValid {
            submitButton.isEnabled = true
            submitButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        } else {
            submitButton.isEnabled = false
            submitButton.backgroundColor = UIColor.lightGray
        }
    }
    
    lazy var acceptanceButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Service Term and Stripe Connect > ", for: .normal)
        //button.titleLabel?.text = "Add Payment"
        //button.backgroundColor = UIColor.magenta
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        //button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleAcceptance), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleAcceptance() {
        delegate?.didTapAcceptanceTerm()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return gender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gender[row]
    }
    
    lazy var submitButton: UIButton = {
        let button = UIButton()
        button.setTitle("Sumbit", for: .normal)
        button.backgroundColor = UIColor.lightGray
        //button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleSubmit() {
        print("handling submit")
        
        guard let firstName = firstNameTextField.text else {return}
        guard let lastName = lastNameTextField.text else {return}
        guard let gender = genderTextField.text else {return}
        guard let phone = phoneNumberText.text else {return}
        guard let dateBirthYear = dateBirthYear.text else {return}
        guard let dateBirthMonth = dateBirthMonth.text else {return}
        guard let dateBirthDay = dateBirthDay.text else {return}
        guard let addressKanjiState = addressStateKanjiTextField.text else {return}
        guard let addressKanjiCity = addressCityKanjiTextField.text else {return}
        guard let addressKanjiTown = addressTownKanjiTextField.text else {return}
        guard let addressKanaState = addressStateKanaTextField.text else {return}
        guard let addressKanaCity = addressCityKanaTextField.text else {return}
        guard let addressKanaTown = addressTownKanaTextField.text else {return}
        guard let addressNumber = addressNumber.text else {return}
        guard let postalNumber = postalNumber.text else {return}
        
        delegate?.didTapSubmit(for: firstName, for: lastName, for: gender, for: phone, for: dateBirthYear, for: dateBirthMonth,  for: dateBirthDay, for: addressKanjiState, for: addressKanjiCity, for: addressKanjiTown, for: addressKanaState, for: addressKanaCity, for: addressKanaTown, for: addressNumber, for: postalNumber)
    }
    
    ///////bank info below//////////////////////////////////////////////
    
    let accountNumber: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Account Number"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    let bankTitle: UILabel = {
        let label = UILabel()
        label.text = "Please provide bank information"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        //label.backgroundColor = UIColor.gray
        label.numberOfLines = 0
        return label
    }()
    
    let routingNumber: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Routing Number"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.borderStyle = .roundedRect
        tf.keyboardType = UIKeyboardType.numberPad
        return tf
    }()
    
    let accountName: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Account Name"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy var submitBankAccount: UIButton = {
        let button = UIButton()
        button.setTitle("Sumbit", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleBank), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func handleBank() {
        print("handing bank account")
        
        guard let accountNumber = accountNumber.text else {return}
        guard let routingNumber = routingNumber.text else {return}
        guard let accountName = accountName.text else {return}
        
        delegate?.didTapBankSubmit(for: accountNumber, for: routingNumber, for: accountName)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
//        pickerView.delegate = self
//        pickerView.dataSource = self
        
         setupView()
    }
    
    func setupView() {
        
        addSubview(becomeHostLabel)
        becomeHostLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(firstNameTextField)
        firstNameTextField.anchor(top: becomeHostLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 15, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(lastNameTextField)
        lastNameTextField.anchor(top: firstNameTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(genderTextField)
        genderTextField.anchor(top: lastNameTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
//        addSubview(genderTextView)
//        genderTextView.anchor(top: lastNameTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 20, width: 0)
        
        addSubview(phoneNumberText)
        phoneNumberText.anchor(top: genderTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(dateBirthYear)
        dateBirthYear.anchor(top: phoneNumberText.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(dateBirthMonth)
        dateBirthMonth.anchor(top: phoneNumberText.bottomAnchor, left: dateBirthYear.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(dateBirthDay)
        dateBirthDay.anchor(top: phoneNumberText.bottomAnchor, left: dateBirthMonth.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(addressStateKanjiTextField)
        addressStateKanjiTextField.anchor(top: dateBirthDay.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(addressCityKanjiTextField)
        addressCityKanjiTextField.anchor(top: dateBirthDay.bottomAnchor, left: addressStateKanjiTextField.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(addressStateKanaTextField)
        addressStateKanaTextField.anchor(top: addressCityKanjiTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(addressCityKanaTextField)
        addressCityKanaTextField.anchor(top: addressCityKanjiTextField.bottomAnchor, left: addressStateKanaTextField.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 5, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(addressTownKanjiTextField)
        addressTownKanjiTextField.anchor(top: addressStateKanaTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(addressTownKanaTextField)
        addressTownKanaTextField.anchor(top: addressTownKanjiTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(addressNumber)
        addressNumber.anchor(top: addressTownKanaTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        addSubview(postalNumber)
        postalNumber.anchor(top: addressNumber.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 0, width: 0)
        
        //addSubview(plusIdButton)
        //plusIdButton.anchor(top: postalNumber.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 0, width: 0)
        
        addSubview(acceptanceButton)
        acceptanceButton.anchor(top: postalNumber.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 200, paddingLeft: 20, paddingBottom: 0, paddingRight: 20, height: 0, width: 0)
        
        addSubview(submitButton)
        submitButton.anchor(top: acceptanceButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 15, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 0, width: 0)
        
        /////////below is bank info///////////
        
        addSubview(bankTitle)
        bankTitle.anchor(top: submitButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(accountNumber)
        accountNumber.anchor(top: bankTitle.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 30, width: 0)
        
        addSubview(routingNumber)
        routingNumber.anchor(top: accountNumber.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 30, width: 0)
        
        addSubview(accountName)
        accountName.anchor(top: routingNumber.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 30, width: 0)
        
        addSubview(submitBankAccount)
        submitBankAccount.anchor(top: accountName.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 15, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
}
