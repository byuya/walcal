//
//  CancelViewCell.swift
//  JobLook
//
//  Created by Akiya Ozawa on 10/28/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

protocol CancelViewCellDelegate {
    func didTapDismiss()
    func didTapCancel()
}
class CancelViewCell: UICollectionViewCell {
    
    var delegate: CancelViewCellDelegate?
    
    var experiment: Experiment? {
        didSet {
            titleLabel.text = experiment?.title
        }
    }
    lazy var dismissButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("X", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 30)
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return button
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tour Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.backgroundColor = UIColor.gray
        return label
    }()
    
    lazy var cancelSendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        button.backgroundColor = UIColor.magenta
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(sendCancel), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func sendCancel() {
        print("try to send cancel")
        delegate?.didTapCancel()
        
    }
    
    @objc fileprivate func handleDismiss() {
        print("handling dismiss")
        delegate?.didTapDismiss()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .brown
        
        setupView()
    }
    
    fileprivate func setupView() {
        
        addSubview(dismissButton)
        dismissButton.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(titleLabel)
        titleLabel.anchor(top: dismissButton.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 15, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(cancelSendButton)
        cancelSendButton.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 20, paddingLeft: 50, paddingBottom: 0, paddingRight: 50, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
