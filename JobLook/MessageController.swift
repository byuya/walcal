//
//  MessageController.swift
//  JobLook
//
//  Created by Akiya Ozawa on H30/08/04.
//  Copyright © 平成30年 Akiya Ozawa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class MessageController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var ref: DatabaseReference!
    var refTransition: DatabaseReference!
    var decrementRef: DatabaseReference!
    
    var handleRef: DatabaseHandle!
    var handleDecrementRef: DatabaseHandle!
    
    var handle: UInt!
    
    var userId: String?
    var countForTotal: UInt?
    var countInMessage: UInt?
    
//    var message: Message? {
//        didSet {
//
//            //navigationItem.title = message?.user?.username
//
//            //to get message content per user
//
//        }
//        
//    }
    var message: Message?
    var messages = [Message]()
    var user: User?
    
    let containerView: UIView = {
        let cv = UIView()
        cv.backgroundColor = .blue
        return cv
    }()
    
   
    var messageId: String?
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
//        navigationItem.hidesBackButton = true
//
//        let newBackButton = UIBarButtonItem(title: "< Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(handleBack))
//
//        navigationItem.leftBarButtonItem = newBackButton
        
        collectionView?.backgroundColor = .white
        
        collectionView?.alwaysBounceVertical = true
        
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        
        collectionView?.keyboardDismissMode = .interactive
        
        tabBarController?.tabBar.isHidden = true
        
        collectionView?.register(MessageCell.self, forCellWithReuseIdentifier: cellId)
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let toId = userId else {return}
        
        print("this is toid", toId)
        
        //initilizer 
        ref = Database.database().reference().child("user-messages").child(uid).child(toId)
        refTransition = Database.database().reference().child("user-messages").child(uid)
        decrementRef = Database.database().reference().child("count").child(uid)
        
//         NotificationCenter.default.addObserver(self, selector: #selector(handleNewTotalCount), name: InboxMessageController.getTotalCountData, object: nil)
        
        setupKeyboardObserver()
        
        //observeMessages()
    
    }
    
    func decrementReadCountWhenOpened() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        handleRef = ref.observe(.childAdded, with: { (snapshot) in
            print("snaps", snapshot.value)
            
            if snapshot.key == "read" {
                
                self.countInMessage = snapshot.value as? UInt
                
                guard let countInMessage = self.countInMessage else {return}
                
                self.decStart(countInMessage: countInMessage, uid: uid)
                self.readCountofTotal(countInMessage: countInMessage, uid: uid)
                
            }
            
        }, withCancel: { (err) in
            print("failed to fetch data", err)
        })
    }
    
        func decmentReadnCountWhenStayed() {
    
            guard let uid = Auth.auth().currentUser?.uid else {return}
    
            handleRef = ref.observe(.childChanged, with: { (snapshot) in
    
                print("snapshots", snapshot.value)
    
                self.countInMessage = snapshot.value as? UInt
    
                guard let countInMessage = self.countInMessage else {return}
    
                self.decStart(countInMessage: countInMessage, uid: uid)
                self.readCountofTotal(countInMessage: countInMessage, uid: uid)
    
            }, withCancel: { (err) in
                print("if failed to observe", err)
                return
            })
        }
    
    func observeMessages() {
        
        //let toId = message?.chatPartnerId()
        guard let uid = Auth.auth().currentUser?.uid, let toId = userId else {return}
        //let userMessagesRef = ref.child(uid).child(toId)
        handleRef = ref.observe(.childAdded, with: { (snapshot) in
            
            //let userMessagesRef = Database.database().reference().child("user-messages").child(uid).child(toId)
            //userMessagesRef.observe(.value, with: { (snapshot) in
            //userMessagesRef.observe(.childAdded, with: { (snapshot) in
            
            if snapshot.key == "read" {

                self.countInMessage = snapshot.value as? UInt

                guard let countInMessage = self.countInMessage else {return}

                //DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    //self.decStart(countInMessage: countInMessage, uid: uid)
                    //self.readCountofTotal(countInMessage: countInMessage, uid: uid)

                //})
            }
            
            //print("trying to read count each user", snapshot.value)
            
            let messageId = snapshot.key
            
            let messagesRef = Database.database().reference().child("messages").child(messageId)
            messagesRef.observeSingleEvent(of: .value, with: { (snapshot) in
                //print(snapshot)
                
                guard let dictionary = snapshot.value as? [String: Any] else {return}
                Database.fetchUserWithUID(uid: toId, completion: { (user) in
                    
                    let message = Message(user: user, dictionary: dictionary)
                    self.navigationItem.title = message.user?.username
                    
                    self.messages.append(message)
                    
                    self.messages.sort(by: { (m1, m2) -> Bool in
                        return m1.creationDate.compare(m2.creationDate) == .orderedAscending
                    })
                    
                    self.collectionView?.reloadData()
                    
                    let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
                    self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
                    
                })
                
            }, withCancel: { (err) in
                print("failed to observe messages node")
            })
            
        }) { (err) in
            print("failed to observe user messages")
        }
    }
    
    func readCountofTotal(countInMessage: UInt, uid: String) {
        
        print("this is totalcount of user has", countInMessage, uid)
        
        decrementRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var currentCountData = currentData.value as? [String: Any] {
                var countData = currentCountData["read"] as! UInt
                
                if countData == 0 {
                    return TransactionResult.abort()
                }
                
                print("this is countData", countData)
                
                countData -= countInMessage
                currentCountData["read"] = countData
                currentData.value = currentCountData
                
                return TransactionResult.success(withValue: currentData)
            }
                return TransactionResult.success(withValue: currentData)
            
        }) { (err, comitted, snapshot) in
            if let err = err {
                print("failed to decrement count", err)
            }
            
            print("if this is successfully executed", snapshot?.value ?? "")
            guard let dictionary = snapshot?.value as? [String: Any] else {return}
            guard let badgeCount = dictionary["read"] as? UInt else {return}
            
            print("badgeCount", badgeCount)
            
            UIApplication.shared.applicationIconBadgeNumber = Int(badgeCount)
            
        }
        
    }
    
    
    fileprivate func decStart(countInMessage: UInt, uid: String) {
        
        print("this count fron each user when backing", countInMessage, uid)
        
                //let decrementRef = Database.database().reference().child("user-messages").child(uid)
                refTransition.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
                    if var totalCountData = currentData.value as? [String: Any] {
                        var totalCount = totalCountData["totalCount"] as! UInt
        
                        print("totalCount", totalCount)
                        
                        if totalCount == 0 {
                            return TransactionResult.abort()
                        }
        
                        totalCount -= countInMessage
                        totalCountData["totalCount"] = totalCount
                        currentData.value = totalCountData
                        //print("making sure if totalcount is incremented by # each user's read count", currentData)
                        return TransactionResult.success(withValue: currentData)
                    }
        
                    return TransactionResult.success(withValue: currentData)
                    
                }
        
                readCount()
    }
    
    func readCount() {
                
        //guard let uid = Auth.auth().currentUser?.uid else {return}
        //guard let toId = userId else {return}
        
        //print("this is the problem", toId)
        
        //let decReadref = Database.database().reference().child("user-messages").child(uid).child(toId)
        ref.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
            if var data = currentData.value as? [String: Any] {
                var count = data["read"] as! Int
                count = 0
                data["read"] = count
                currentData.value = data
                //print("let me this what value prints out", currentData)
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }
    }
    
    func setupKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
    }
    
    @objc func handleKeyboardDidShow() {
        if messages.count > 0 {
            let indexPath = IndexPath(item: messages.count - 1, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .top, animated: true)
        }
       
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MessageCell
        cell.message = messages[indexPath.item]
        
        setupCell(cell: cell)
        
        //cell.bubbleWidthAnchor?.constant = 50
        if let textMessage = messages[indexPath.item].messageText {
            cell.bubbleWidthAnchor?.constant = estimatedFrameForText(text: textMessage).width + 35
        
        }
        
        return cell
    }
    
    private func setupCell(cell: MessageCell) {
        
        //guard let message = message else {return}
        if let profileImageUrl = message?.user?.profileImageUrl {
            //self.message?.user = user
            cell.profileImageView.loadImage(urlString: profileImageUrl)

        }
        
        if cell.message?.fromId == Auth.auth().currentUser?.uid {
            cell.bubbleView.backgroundColor = MessageCell.blueColor
            cell.messageView.textColor = .white
            cell.profileImageView.isHidden = true
            
            cell.bubbleRightAnchor?.isActive = true
            cell.bubbleLeftAnchor?.isActive = false
            
        } else {
            cell.bubbleView.backgroundColor = UIColor.rgb(red: 240, green: 240, blue: 240)
            cell.messageView.textColor = .black
            cell.profileImageView.isHidden = false
            
            cell.bubbleRightAnchor?.isActive = false
            cell.bubbleLeftAnchor?.isActive = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 80
        //get estimated height somehow
        if let text = messages[indexPath.item].messageText  {
            height = estimatedFrameForText(text: text).height + 30
        }
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    private func estimatedFrameForText(text: String) -> CGRect {
        
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], context: nil)
    }
    
     lazy var messageContainerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = .white
        containerView.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
        
        let submitButton = UIButton(type: .system)
        submitButton.setTitle("Send", for: .normal)
        submitButton.setTitleColor(.black, for: .normal)
        submitButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        submitButton.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        containerView.addSubview(submitButton)
        submitButton.anchor(top: containerView.topAnchor, left: nil, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 8, height: 0, width: 80)
        
//        let textField = UITextField()
//        textField.placeholder = "メッセージ..."
        containerView.addSubview(messageTextField)
        messageTextField.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: submitButton.leftAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230)
        containerView.addSubview(separatorView)
        separatorView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        return containerView
    }()
    
    let messageTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Leave a message..."
        return textField
    }()
    
    static let updateMessageCount = NSNotification.Name(rawValue: "messageUpdate")
    
    @objc func handleSubmit() {
        
        print("Inserting message", messageTextField.text ?? "")
        
        guard let fromId = Auth.auth().currentUser?.uid else {return}
        guard let toId = userId else { return }
        let creationDate = Date().timeIntervalSince1970
        
        let values = ["messageText": messageTextField.text ?? "", "creationDate": creationDate, "fromId": fromId, "toId": toId] as [String : Any]
        
        let ref = Database.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        
        childRef.updateChildValues(values) { (err, ref) in
            
            print(values)
            if err != nil {
                return
            }
            
            self.messageTextField.text = nil
            
            let userMessageRef = Database.database().reference().child("user-messages").child(fromId).child(toId)
            
            guard let messageId = childRef.key else {return}
            
            //userMessageRef.updateChildValues([messageId: 1])
            userMessageRef.updateChildValues([messageId: 1], withCompletionBlock: { (err, ref) in
                if let err = err {
                    print("failed to stored usermessageid", err)
                    return
                }
                
                print("Successfully stored usermessageif")
                
                let recipientUserMessagesRef = Database.database().reference().child("user-messages").child(toId).child(fromId)
                recipientUserMessagesRef.updateChildValues([messageId: 1], withCompletionBlock: { (err, ref) in
                    if let err = err {
                        print("failed to store recipientMessageid", err)
                        return
                    }
                    
                    print("successfuly stored recipientMessageid")
                    
                    let ref = Database.database().reference().child("user-messages").child(toId).child(fromId)
                    ref.runTransactionBlock { (currentData: MutableData) -> TransactionResult in
                        if var data = currentData.value as? [String: Any] {
                            var count = data["read"] as! Int
                            count += 1
                            data["read"] = count
                            currentData.value = data
                            print("let me this what value prints out", currentData)
                            return TransactionResult.success(withValue: currentData)
                        }
                        
                        return TransactionResult.success(withValue: currentData)
                    }
                    
//                    let totalCountRef = Database.database().reference().child("user-messages").child(toId)
//                    totalCountRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
//                        if var totalCountData = currentData.value as? [String: Any] {
//                            var totalCount = totalCountData["totalCount"] as! Int
//                            totalCount += 1
//                            totalCountData["totalCount"] = totalCount
//                            currentData.value = totalCountData
//                            print("making sure if totalcount is incremented by 1", currentData)
//                            return TransactionResult.success(withValue: currentData)
//                        }
//                            return TransactionResult.success(withValue: currentData)
//                    })

            })
                
        })
            
        }
    }
    
    override var inputAccessoryView: UIView? {
        get {
        
        return messageContainerView
            
        }

    }

    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        observeMessages()
        decmentReadnCountWhenStayed()
        decrementReadCountWhenOpened()
        
        print("if this is working")
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(moveToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(moveToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func moveToBackground() {
        print("app ented background")
        //ref.removeAllObservers()
        ref.removeObserver(withHandle: handleRef)
    }
    
    @objc func moveToForeground() {
        print("app entered foreground")
        //observeMessages()
        decrementReadCountWhenOpened()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view disppearing from MessageController")
        ref.removeAllObservers()
        //refTransition.removeAllObservers()
        
        //ref.removeObserver(withHandle: handleRef)
        
         //NotificationCenter.default.removeObserver(self)
    }
    
//    func observeMessages2() {
//
//        guard let uid = Auth.auth().currentUser?.uid, let toId = userId else {return}
//
//        //let userMessagesRef = Database.database().reference().child("user-messages").child(uid).child(toId)
//        //userMessagesRef.observe(.value, with: { (snapshot) in
//            //userMessagesRef.observe(.value, with: { (snap) in
//
//            handleRef = ref.observe(.value, with: { (snap) in
//
//            print("this is snapshot", snap.value)
//
//            guard let messageDic = snap.value as? [String: Any] else {return}
//            messageDic.forEach({ (key, value) in
//
//                if key == "read" {
//                    self.countInMessage = value as? UInt
//
//                    guard let countInMessage = self.countInMessage else {return}
//
//                    //DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
//                        self.decStart(countInMessage: countInMessage, uid: uid)
//
//                    //})
//                }
//
//                print("this is userids", key)
//
//                let messageId = key
//
//                let messagesRef = Database.database().reference().child("messages").child(messageId)
//                messagesRef.observeSingleEvent(of: .value, with: { (snapshot) in
//                    //print(snapshot)
//
//                    guard let dictionary = snapshot.value as? [String: Any] else {return}
//                    Database.fetchUserWithUID(uid: toId, completion: { (user) in
//
//                        let message = Message(user: user, dictionary: dictionary)
//                        self.navigationItem.title = message.user?.username
//
//                        self.messages.append(message)
//
//                        self.messages.sort(by: { (m1, m2) -> Bool in
//                            return m1.creationDate.compare(m2.creationDate) == .orderedAscending
//                        })
//
//                        self.collectionView?.reloadData()
//
//                        let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
//                        self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
//
//                    })
//
//                }, withCancel: { (err) in
//                    print("failed to observe messages node")
//                })
//
//            })
//
//        }) { (err) in
//            print("failed to observe usermessages", err)
//        }
//
//    }
    
    //    @objc fileprivate func handleBack() {
    //
    //        //decrementTotalAfterBack()
    //
    //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
    //             //self.readCount()
    //             self.navigationController?.popToRootViewController(animated: true)
    //
    //        })
    //
    //    }
    
    //    fileprivate func decrementTotalAfterBack() {
    //
    //        guard let uid = Auth.auth().currentUser?.uid else {return}
    //        guard let toId = userId else {return}
    //
    //        //let ref = Database.database().reference().child("user-messages").child(uid).child(toId)
    //        //let decTotalRef = ref.child(uid).child(toId)
    //
    //        let decrementRef = Database.database().reference().child("user-messages").child(uid).child(toId)
    //        //decrementRef.queryLimited(toLast: 1).observeSingleEvent(of: .value, with: { (snapshot) in
    //        decrementRef.observe(.childAdded, with: { (snapshot) in
    //
    //        //handleRef =  ref.queryLimited(toLast: 1).observe(.value, with: { (snapshot) in
    //
    //        //decTotalRef.queryLimited(toLast: 1).observe(.value, with: { (snapshot) in
    //
    //        //ref.observe(.childAdded, with: { (snapshot) in
    //
    //            print("snap value", snapshot.value)
    //
    //            guard let dict = snapshot.value as? [String: Any] else {return}
    //            dict.forEach({ (key, value) in
    //
    //                if key == "read" {
    //
    //                    self.countInMessage = value as? UInt
    //
    //                    guard let countInMessage = self.countInMessage else {return}
    //
    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
    //                        self.decStart(countInMessage: countInMessage, uid: uid)
    //
    //                    })
    //                }
    //
    //            })
    //
    //        }) { (err) in
    //            print("failed to fetch data", err)
    //        }
    //
    //    }

    
}


