//
//  DepositHeader.swift
//  JobLook
//
//  Created by Akiya Ozawa on 12/11/30 H.
//  Copyright © 30 Heisei Akiya Ozawa. All rights reserved.
//

import UIKit

protocol DepositCellDelegate {
    func didTapDeposit()
}
class DepositHeader: UICollectionViewCell {
    
    var delegate: DepositCellDelegate?
    
    let salesLabel: UILabel = {
        let label = UILabel()
        label.text = "Total amount you can deposit "
        label.font = UIFont.boldSystemFont(ofSize: 18)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.text = "¥1,000 "
        label.font = UIFont.boldSystemFont(ofSize: 30)
        //label.backgroundColor = UIColor.gray
        return label
    }()
    
    lazy var depositReqButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Request Deposit", for: .normal)
        button.backgroundColor = UIColor.magenta
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleDeposit), for: .touchUpInside)
        return button
    }()
    
    @objc func handleDeposit() {
        print("handling deposit")
        delegate?.didTapDeposit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .yellow
        
        setupView()
    }
    
    func setupView() {
        
        addSubview(salesLabel)
        salesLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 15, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0, width: 0)
        
        addSubview(priceLabel)
        priceLabel.anchor(top: topAnchor, left: salesLabel.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, height: 0, width: 0)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        addSubview(seperatorView)
        seperatorView.anchor(top: priceLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, height: 0.5, width: 0)
        
        addSubview(depositReqButton)
        depositReqButton.anchor(top: seperatorView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 15, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, height: 50, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
